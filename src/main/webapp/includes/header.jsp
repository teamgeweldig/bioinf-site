<%--
  Authors Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  A default header used on all pages to make changes easier (you don't have to change it on every page), also contains
  the navbar.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>${requestScope.title}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans:100,300,400" rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <%-- Add logo--%>
        <img class="logo" src="${pageContext.request.contextPath}/img/logo.png"/>
        <%-- Add menu items--%>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.request.contextPath}/promotion#contactSection">Contact</a>
            </li>
        </ul>
        <ul class="navbar-nav" id="login-navbar">
            <li class="nav-item">
                <%-- Checks if the user is on the student page or the promotion page--%>
                <c:choose>
                    <%-- If the user is on the student page, the text will redirect to the promotion page--%>
                    <c:when test="${cookie['student'].value eq 'yes' || requestScope.title != 'Bioinf.nl - Promotie'}">
                        <a class="nav-link" href="${pageContext.request.contextPath}/promotion" role="button">
                            Bekijk de promotie pagina
                        </a>
                    </c:when>
                    <%-- If the student is on the promotion page, the text will redirect to the student page--%>
                    <c:otherwise>
                        <a class="nav-link" href="${pageContext.request.contextPath}/student" role="button">
                            Ik ben student
                        </a>
                    </c:otherwise>
                </c:choose>
                <%--<ul id="login-dp" class="dropdown-menu dropdown-menu-right">--%>
                <%--<li>--%>
                <%--<div class="row">--%>
                <%--<div class="col-md-12">--%>
                <%--<form class="form" role="form" method="post" action="" accept-charset="UTF-8" id="login-nav">--%>
                <%--<div class="form-group">--%>
                <%--<label class="sr-only" for="username">Username</label>--%>
                <%--<input type="text" class="form-control" id="username" placeholder="Username" required>--%>
                <%--</div>--%>
                <%--<div class="form-group">--%>
                <%--<label class="sr-only" for="exampleInputPassword2">Password</label>--%>
                <%--<input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required>--%>
                <%--<div class="help-block text-right"><a href="">Forget the password ?</a></div>--%>
                <%--</div>--%>
                <%--<div class="form-group">--%>
                <%--<button type="submit" class="btn btn-dark btn-block">Sign in</button>--%>
                <%--</div>--%>
                <%--<div class="checkbox">--%>
                <%--<label>--%>
                <%--<input type="checkbox"> Keep me logged-in--%>
                <%--</label>--%>
                <%--</div>--%>
                <%--</form>--%>
                <%--</div>--%>
                <%--</div>--%>
                <%--</li>--%>
                <%--</ul>--%>
            </li>
        </ul>
    </div>
</nav>
