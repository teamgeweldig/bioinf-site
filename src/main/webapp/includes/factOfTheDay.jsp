<%--
  Authors Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  Contains the content for the fact of the day block, changes every day and receives its contents from the database
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="text-center">
    <%-- Add fact information --%>
    <div class="fact-text">
        <c:if test="${requestScope.fact.title != null}">
            <h1 class="mt-2">${requestScope.fact.title}</h1>
        </c:if>
        <p>${requestScope.fact.content}</p>
        <c:if test="${requestScope.fact.source != null}">
            <p>Bron: <a href="">${requestScope.fact.source}</a></p>
        </c:if>
    </div>

    <%--Add image if available --%>
    <c:if test="${requestScope.fact.image != null}">
        <c:choose>
            <c:when test="${fn:startsWith(requestScope.fact.image, 'http')}">
                <img class="fodd-image"
                     src="${requestScope.fact.image}"/>
            </c:when>
            <c:otherwise>
                <img class="fodd-image"
                     src="${pageContext.request.contextPath}/${initParam.factImagePath}/${requestScope.fact.image}"/>
            </c:otherwise>
        </c:choose>
    </c:if>

</div>

