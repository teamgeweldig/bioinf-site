<%--
  Authors: Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  The JSP for the redirect links, these links can be shortcuts to commonly used applications. This data is also
  received from a database table.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="container">
    <%-- Header of the links block--%>
    <h1 class="linkTitle">
        <i class="fa fa-external-link" aria-hidden="true"></i>Links</h1>
</div>
<hr class="border-2px">
<div class="container mb-1 ml-1">
    <div class="wraper">
        <%-- Creates a link for each link object--%>
        <c:forEach items="${requestScope.linkItems}" var="linkObject">
            <div class="inf-top-margin">
                    <%-- Creating the link part--%>
                <a class="li" href="${linkObject.address}">
                    <div class=top>
                        <c:choose>
                            <c:when test="${fn:startsWith(linkObject.image, 'http')}">
                                <img class="linkimg" src="${linkObject.image}">
                            </c:when>
                            <c:otherwise>
                                <img class="linkimg" src="img/links/${linkObject.image}">
                            </c:otherwise>
                        </c:choose>

                    </div>
                        <%-- Part that moves when hovering over the link--%>
                    <div class="slider bottom">
                        <div class="linkname">
                                ${linkObject.name}
                        </div>
                    </div>
                </a>
            </div>
        </c:forEach>
    </div>
</div>