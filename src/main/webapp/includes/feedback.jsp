<%--
  Authors Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  The content for the feedback modal, sents the feedback to the central email adres specified in web.xml.
--%>

<%-- Add feedback form in a modal--%>
<div class="modal fade" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog team-dialog" role="document">
        <div class="modal-content team-content">

            <div class="modal-header">
                <h1>Feedback</h1>
            </div>
            <div class="modal-body">
                <div id="message">
                    <%-- Form containing all options for the feedback form --%>
                    <form id="frmContact" name="frmContact" action="feedback" method="POST">
                        <div class="label">Name:</div>
                        <div class="field">
                            <input class="inputField required" type="text" id="pp-name" name="name"
                                   placeholder="enter your name here" title="Please enter your name"
                                   aria-required="true" required>
                        </div>
                        <div class="label">Email:</div>
                        <div class="field">
                            <input class="inputField required email" type="text" id="pp-email" name="email"
                                   placeholder="enter your email address here"
                                   title="Please enter your email address"
                                   aria-required="true" required>
                        </div>
                        <div class="label">Subject:</div>
                        <div class="field">
                            <input class="inputField required" type="text" id="pp-phone" name="subject"
                                   placeholder="Subject"
                                   title="Subject"
                                   aria-required="true" required>
                        </div>
                        <div class="label">Message:</div>
                        <div class="field">
						<textarea class="inputTextarea" id="about-project" name="message"
                                  placeholder="enter your message here"></textarea>
                        </div>
                        <div id="mail-status"></div>
                        <input class="inputField" type="submit" name="submit" value="Verstuur"
                               id="send-message" style="clear: both;">
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="close" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

</div>