<%--
  Authors Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  JSP for displaying all the news items (limited to 100 items in the DAO) with their corresponding modal.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="top-fixed">
    <div class="container">
        <h1 class="newsTitle"><i class="fa fa-newspaper-o" aria-hidden="true"></i>
            <span class="normal">BioInformatica</span> <b>Nieuws</b>
        </h1>
    </div>
    <hr class="border-2px">
</div>

<ul id="news">
    <%-- Iterate over list with news items --%>
    <c:forEach var="message" items="${requestScope.news}">
        <div class="media">
            <div class="media-left media-middle"></div>
            <%-- Check if there is a image available --%>
            <c:if test="${not empty message.image}">
                <c:choose>
                    <c:when test="${fn:startsWith(message.image, 'http')}">
                        <img class="newsMedia-object"
                             src="${message.image}"
                             alt="${message.id}-newsItemImage"/>
                    </c:when>
                    <c:otherwise>
                        <img class="newsMedia-object"
                             src="${pageContext.request.contextPath}/${initParam.newsImagesPath}/${message.image}"
                             alt="${message.id}-newsItemImage"/>
                    </c:otherwise>
                </c:choose>
            </c:if>

            <div class="newsMedia-body">
                <!-- Trigger/open the modal -->
                <a id="${message.id}"><h5>${message.title}</h5></a>

                <%-- Truncate if content is longer than 20 characters --%>
                <a id="${message.id}" type="text" data-target="#${message.id}-content"></a>
                <p class="shortContent newsContent">${message.content}</p>

                <!-- Trigger modal -->
                <a id="${message.id}" type="text" class="buildModal" data-toggle="modal"
                   data-target="#${message.id}-newsId">
                    <p class="toNewsModal ">Lees meer</p>
                </a>
            </div>
        </div>

        <!-- The Modal Itself -->
        <div id="${message.id}-newsId" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <%-- Modal view --%>
            <div class="modal-dialog" role="document">
                    <%-- All Modal Content (incl header, content and footer) class--%>
                <div class="modal-content">
                        <%-- Close Button --%>
                    <p>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </p>

                    <hr/>
                    <%-- Background Image --%>
                    <c:if test="${not empty message.image}">
                        <div class="bg-image">
                            <c:choose>
                                <c:when test="${fn:startsWith(message.image, 'http')}">
                                    <input type="hidden"
                                           value="${message.image}"/>
                                </c:when>
                                <c:otherwise>
                                    <input type="hidden"
                                           value="${pageContext.request.contextPath}/${initParam.newsImagesPath}/${message.image}"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </c:if>

                    <div class="modal-header">
                            <%-- Modal Header --%>
                        <div class="row header text-center">
                            <div class="col-md-12">
                                <h1 class="title">${message.title}</h1>
                            </div>
                        </div>
                    </div>

                        <%-- Modal Contents --%>
                    <div class="modal-body">
                        <p class="content">${message.content}</p>
                    </div>

                        <%-- Modal Footer with close button --%>
                    <div class="modal-footer">
                        <button type="button" class="close" data-dismiss="modal">Close</button>
                    </div>
                <%-- End of All Modal Content --%>
                </div>
            <%-- End of Modal View --%>
            </div>
        <%-- End of Modal --%>
        </div>
        <hr class="border-2px">
    </c:forEach>
</ul>

