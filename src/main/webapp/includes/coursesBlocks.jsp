<%--
  Authors Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  The JSP page for displaying all course blocks with the corresponding modal, creates the blocks based on the data from
  the database.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="d-flex justify-content-center mt-4">
    <div class="no-scroll" id="coursesBlocks">
        <div class="grid blank-background">
            <div class="grid-sizer"></div>
            <%-- Creates course blocks and places them in a grid--%>
            <c:forEach var="i" end="9" items="${requestScope.courseItems}" varStatus="counter">
                <div class="grid-item grid-item--height-courses-blocks add-bg-image">
                    <input type="hidden" name="img" value="${i.image}">
                    <div class="hovereffect d-flex align-items-center justify-content-center">
                        <h2 class="text-center">${i.title}</h2>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <p>
                                <a href="#" data-toggle="modal" data-target="#theModalCourses${i.id}">
                                    Meer informatie
                                </a>
                            </p>
                        </div>
                    </div>
                    <div class="modal fade" id="theModalCourses${i.id}" role="dialog">
                        <div class="modal-dialog modal-lg">
                                <%-- Modal content--%>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">${i.title}</h4>
                                </div>
                                <div class="modal-body">
                                    <ul id="vakken-${i.id}" class="vakken-ul">${i.subjects}</ul>
                                    <p class="desc description">${i.description}</p>
                                    <input type="hidden" name="subj-desc" value='${i.subjectDescriptions}'>
                                </div>
                                <div class="modal-footer">
                                        <%--Modal close button--%>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
            <%-- Creates course blocks and places them in a grid (half year blocks such as minor)--%>
            <c:forEach var="i" begin="10" items="${requestScope.courseItems}" varStatus="counter">
                <div class="grid-item grid-item--height-courses-blocks grid-item--width2-courses-blocks add-bg-image">
                    <input type="hidden" name="img" value="${i.image}">
                    <div class="hovereffect d-flex align-items-center justify-content-center">
                        <h2 class="text-center">${i.title}</h2>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <p>
                                <a href="#" data-toggle="modal" data-target="#theModalCourses${i.id}">
                                    Meer informatie
                                </a>
                            </p>
                        </div>
                    </div>
                    <div class="modal fade" id="theModalCourses${i.id}" role="dialog">
                        <div class="modal-dialog modal-lg">
                                <%-- Modal content--%>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">${i.title}</h4>
                                </div>
                                <div class="modal-body">
                                    <ul id="vakken-${i.id}" class="vakken-ul">${i.subjects}</ul>
                                    <p class="desc description">${i.description}</p>
                                    <input type="hidden" name="subj-desc" value='${i.subjectDescriptions}'>
                                </div>
                                <div class="modal-footer">
                                        <%--Modal close button--%>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>