<%--
  Authors Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  Displays the 'middle promotion blocks' (after the first slideshow), containing all kinds of information about the
  study (based on the information in the database).
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="d-flex justify-content-center mt-4">
    <div class="no-scroll" id="informationBlocks">
        <%-- Create a grid containing all the information blocks. The size of the blocks if requested from the
        database--%>
        <div class="grid blank-background">
            <div class="grid-sizer"></div>
            <c:forEach var="i" begin="2" items="${requestScope.promotionBlocks}" varStatus="counter">
                <c:if test="${i.width eq null or i.width eq 1}">
                    <c:set var="width" value=""/>
                </c:if>
                <c:if test="${i.height eq null or i.height eq 1}">
                    <c:set var="height" value=""/>
                </c:if>
                <c:if test="${i.height >= 2}">
                    <c:set var="height" value="grid-item--height${i.height}"/>
                </c:if>
                <c:if test="${i.width >= 2}">
                    <c:set var="width" value="grid-item--width${i.width}"/>
                </c:if>

                <%-- Check if there is a image available (image name or link)--%>
                <div class="grid-item <c:out value = "${width}"/> <c:out value = "${height}"/> add-bg-image">
                    <c:set var="imageFileName" value="${i.imageFileName}"/>
                    <c:choose>
                        <c:when test="${i.imageFileName == null}">
                            <input type="hidden" name="img" value="${i.imageFileName}">
                        </c:when>
                        <c:when test="${fn:startsWith(imageFileName, 'http')}">
                            <input type="hidden" name="img" value="${i.imageFileName}">
                        </c:when>
                        <c:otherwise>
                            <input type="hidden" name="img"
                                   value="${pageContext.request.contextPath}/${initParam.promotionImage}/${i.imageFileName}"
                        </c:otherwise>
                    </c:choose>

                    <%--Add hover effect to informations blocks with trigger to modal--%>
                    <div class="hovereffect d-flex align-items-center justify-content-center">
                        <h2 class="text-center">${i.title}</h2>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <p>
                                <a href="#" data-toggle="modal" data-target="#theModal${i.id}">Meer informatie</a>
                            </p>
                        </div>
                    </div>

                    <%-- Modal --%>
                    <div class="modal fade" id="theModal${i.id}" role="dialog">
                        <div class="modal-dialog">
                            <%-- Modal content--%>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">${i.title}</h4>
                                </div>
                                <div class="modal-body">
                                    <h3>${i.content}</h3>
                                </div>
                                <div class="modal-footer">
                                    <%--close button--%>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>