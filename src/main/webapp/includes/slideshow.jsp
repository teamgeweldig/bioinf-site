<%--
  Authors Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  JSP for creating the slideshows, receives the image and title data from the database.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="slide" value="${param.slideshowItems}"/>
<div id="${param.slideshowItems}" class="carousel slide" data-ride="carousel">
    <%-- Create slider indicators (show the amount of slideshow items)--%>
    <ol class="carousel-indicators">
        <c:forEach items="${requestScope[slide]}" varStatus="counter">
            <c:if test="${counter.index eq 0}">
                <li data-target="#${param.slideshowItems}" data-slide-to="${counter.index}" class="active"></li>
            </c:if>
            <c:if test="${counter.index > 0}">
                <li data-target="#${param.slideshowItems}" data-slide-to="${counter.index}"></li>
            </c:if>
        </c:forEach>
    </ol>

    <%--Create the slideshow items--%>
    <div class="carousel-inner">
        <c:forEach var="item" items="${requestScope[slide]}" varStatus="counter">

            <%-- Create first slider item (active on the moment the webpage starts)--%>
            <c:if test="${counter.index eq 0}">
                <div class="carousel-item active">
                    <c:set var="imageFileName" value="${item.imageFileName}"/>
                    <c:choose>
                        <c:when test="${fn:startsWith(imageFileName, 'http')}">
                            <img class="d-block w-100 heightCarouselSlideShow" src="${item.imageFileName}">
                        </c:when>
                        <c:otherwise>
                            <img class="d-block w-100 heightCarouselSlideShow"
                                 src="${pageContext.request.contextPath}/${initParam.slideshowImagePath}/${item.imageFileName}">
                        </c:otherwise>
                    </c:choose>
                    <div class="carousel-caption d-none d-md-block">
                        <h3>${item.title}</h3>
                        <p>${item.subtitle}</p>
                    </div>
                </div>
            </c:if>

            <%-- Create the next slider items (not active on the moment the webpage starts)--%>
            <c:if test="${counter.index > 0}">
                <div class="carousel-item">
                    <c:set var="imageFileName" value="${item.imageFileName}"/>
                    <c:choose>
                        <c:when test="${fn:startsWith(imageFileName, 'http')}">
                            <img class="d-block w-100 heightCarouselSlideShow" src="${item.imageFileName}">
                        </c:when>
                        <c:otherwise>
                            <img class="d-block w-100 heightCarouselSlideShow"
                                 src="${pageContext.request.contextPath}/${initParam.slideshowImagePath}/${item.imageFileName}">
                        </c:otherwise>
                    </c:choose>
                    <div class="carousel-caption d-none d-md-block">
                        <h3>${item.title}</h3>
                        <p>${item.subtitle}</p>
                    </div>
                </div>
            </c:if>
        </c:forEach>
    </div>

    <%--Create previous button--%>
    <a class="carousel-control-prev" href="#${param.slideshowItems}" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>

    <%--Create next button--%>
    <a class="carousel-control-next" href="#${param.slideshowItems}" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
