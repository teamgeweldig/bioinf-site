<%--
  Authors Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  A JSP template which is used for the agenda block, uses the agenda items from the DB
--%>
<%@ page contentType="text/html;charset=UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="container" id="events-container">
    <div class="top-fixed">
        <div class="container">
            <h2 class="event-title"><i class="fa fa-calendar" aria-hidden="true"></i>
                Agenda</h2>
        </div>
        <hr class="border-2px">
    </div>
    <%--For each event item in the agenda objects list is added to an event block--%>
    <c:forEach var="event" items="${requestScope.events}">
        <%--Separates the event date--%>
        <c:set var="splittedDate" value="${fn:split(event.date, ' ')}"/>
        <div class="event-div">
            <div class="date-div">
                <div class="date-top">
                    <p>${splittedDate[1]}</p>
                </div>
                <div class="date-bottom">
                    <p>${splittedDate[0]}</p>
                </div>
            </div>
            <div class="date-text">
                <h4 class="mt-0">${event.title}</h4>
                <p class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> ${event.location}</p>
                <p>${event.content}</p>
            </div>
            <%-- Checks if there is a image for the event, will give alternative if not--%>
            <c:choose>
                <c:when test="${fn:startsWith(event.image, 'http')}">
                    <img class="date-image" src="${event.image}">
                </c:when>
                <c:otherwise>
                    <img class="date-image"
                         src="${pageContext.request.contextPath}/${initParam.eventImage}/${event.image}">
                </c:otherwise>
            </c:choose>
        </div>
        <hr class="border-2px"/>
    </c:forEach>
</div>
