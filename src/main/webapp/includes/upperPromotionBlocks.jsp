<%--
  Authors Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  The top 2 promotion blocks for very relevant information, the data is received from the database
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="d-flex justify-content-center mt-4">
    <div class="no-scroll" id="upperBlocks">
        <%--Create grid containing the first two blocks of the promotion page--%>
        <div class="grid blank-background">
            <div class="grid-sizer"></div>
            <%-- Create block for first two blocks--%>
            <c:forEach var="i" end="1" items="${requestScope.promotionBlocks}" varStatus="counter">
                <div class="grid-item grid-item--height-upper-blocks add-bg-image">
                    <c:set var="imageFileName" value="${i.imageFileName}"/>

                    <%-- Check if there is an image, when there is check if it is a link or a filename--%>
                    <c:choose>
                        <c:when test="${empty i.imageFileName}">
                            <input type="hidden" name="img" value="${i.imageFileName}">
                        </c:when>
                        <c:when test="${fn:startsWith(imageFileName, 'http')}">
                            <input type="hidden" name="img" value="${i.imageFileName}">
                        </c:when>
                        <c:otherwise>
                            <input type="hidden" name="img"
                                   value="${pageContext.request.contextPath}/${initParam.promotionImage}/${i.imageFileName}"
                        </c:otherwise>
                    </c:choose>

                    <%-- Add hover effect to block and trigger to modal--%>
                    <div class="hovereffect d-flex align-items-center justify-content-center">
                        <h2 class="text-center">${i.title}</h2>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <p>
                                <a href="#" data-toggle="modal" data-target="#theModal${i.id}">Meer informatie</a>
                            </p>
                        </div>
                    </div>

                    <%--Modal by the block--%>
                    <div class="modal fade" id="theModal${i.id}" role="dialog">
                        <div class="modal-dialog">
                                <%-- Modal content--%>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">${i.title}</h4>
                                </div>
                                <div class="modal-body">
                                    <h3>${i.content}</h3>
                                </div>
                                <div class="modal-footer">
                                        <%--Modal close button--%>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
