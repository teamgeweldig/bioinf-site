<%--
  Authors: Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  Blocks containing redirect information about team members of bioinformatics, schedules and status of the pc's.
  Part of student page.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>

<div class="row height-100 bg-primary">
    <%-- Block to link to courses section of the promotion page --%>
    <div class="col-md-4 d-flex align-items-center justify-content-center add-bg-image hovereffect p-0 ">
        <h2 class="text-center">Thema's</h2>
        <input type="hidden" name="img" value="https://www.stockvault.net/data/2017/04/25/234715/preview16.jpg">
        <div class="overlay d-flex align-items-center justify-content-center">
            <p>
                <a href="${pageContext.request.contextPath}/promotion#coursesBlocks">Meer informatie</a>
            </p>
        </div>
    </div>

    <%-- Block to link to the room schedules--%>
    <div class="col-md-4 d-flex align-items-center justify-content-center hovereffect add-bg-image p-0 ">
        <h2 class="text-center">Bekijk hier de zaalroosters</h2>
        <input type="hidden" name="img"
               value="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg">
        <div class="overlay d-flex align-items-center justify-content-center">
            <p>
                <a href="https://bioinf.nl/zaalroosters">Meer informatie</a>
            </p>
        </div>
    </div>

    <%-- Block to link to the status of all computers--%>
    <div class="col-md-4 d-flex align-items-center justify-content-center hovereffect add-bg-image p-0 ">
        <h2 class="text-center">Status PC's</h2>
        <input type="hidden" name="img" value="https://static.pexels.com/photos/577585/pexels-photo-577585.jpeg">
        <div class="overlay d-flex align-items-center justify-content-center">
            <p>
                <a href="https://bioinf.nl/~piet/status/">Meer informatie</a>
            </p>
        </div>
    </div>
</div>
