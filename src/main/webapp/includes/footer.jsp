<%--
  Authors Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  A universal footer with the same goal as the header, also contains the sticky bar at the bottom containg the copyright
   and author information.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <div class="container footer">
        <p>
            <i class="fa fa-copyright" aria-hidden="true"></i> 2017:
            <a href="https://www.linkedin.com/in/beau-elting-9a5886105" target="_blank">Beau Elting</a>, Wietse Reitsma,
            <a href="https://www.linkedin.com/in/kimberley-kalkhoven-863537137/" target="_blank">Kimberley Kalkhoven</a> and
            <a href="https://www.linkedin.com/in/sven-hakvoort-3199a0101/" target="_blank">Sven Hakvoort</a>
        </p>
    </div>
</body>

<script src="${pageContext.request.contextPath}/js/libs/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
        integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
        crossorigin="anonymous"></script>
<script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/news.js" type="text/javascript"></script>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="${pageContext.request.contextPath}/js/informationBlocks.js"></script>
<script src="${pageContext.request.contextPath}/js/links.js"></script>
<script src="${pageContext.request.contextPath}/js/modalEvents.js"></script>
<script src="${pageContext.request.contextPath}/js/backToTop.js"></script>

</html>

