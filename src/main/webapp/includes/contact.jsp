<%--
  Authors Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  The JSP for displaying the contact section, containing a map and three blocks in which the contact information is
  placed.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<div class="animated fadeIn pr-0 pl-0 m-0">
    <div id="contactSection" class="container-fluid pr-0 pl-0">
        <%--Map view of location school--%>
        <iframe width="100%" height="200px;" frameborder="0"
                src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJV9B-hRvNyUcR6kKwIQGd-sQ&key=AIzaSyAf64FepFyUGZd3WFWhZzisswVx2K37RFY"
                allowfullscreen></iframe>
        <%--Contant details--%>
        <div class="container-fluid second-portion pr-0 pl-0">
            <div class="row" id="row-contact">
                <%--Mail and website--%>
                <div class="col">
                    <div class="box">
                        <div class="icon">
                            <div class="image"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                            <div class="info">
                                <h3 class="title">MAIL & WEBSITE</h3>
                                <p>
                                    <i class="fa fa-envelope" aria-hidden="true"></i> &nbsp
                                    <a href="mailto:secretariaat_ilst@org.hanze.nl">secretariaat_ilst@org.hanze.nl​</a>
                                    <br>
                                    <i class="fa fa-globe" aria-hidden="true"></i> &nbsp www.bioinf.nl
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <%--Telephone number--%>
                <div class="col">
                    <div class="box">
                        <div class="icon">
                            <div class="image"><i class="fa fa-mobile" aria-hidden="true"></i></div>
                            <div class="info">
                                <h3 class="title">INSTITUUT VOOR LIFE SCIENCE & TECHNOLOGY</h3>
                                <p>
                                    <i class="fa fa-mobile" aria-hidden="true"></i> &nbsp <a
                                        href="tel:+31 (0)50 595 45 69">+31 (0)50 595 45 69</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <%--Adress--%>
                <div class="col">
                    <div class="box">
                        <div class="icon">
                            <div class="image"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                            <div class="info">
                                <h3 class="title">ADRES</h3>
                                <p>
                                    <i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp Van Doorenveste <br/>
                                    Zernikeplein 11<br/>
                                    9747 AS Groningen
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
