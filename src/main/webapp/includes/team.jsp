<%--
  Authors Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  JSP for showing information about the team members, shown in a modal when specific button linking to this modal is clicked
  Information about the team members is received from the database
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page contentType="text/html;charset=UTF-8" %>

<%--Team modal--%>
<div class="modal fade" id="teamModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog team-dialog" role="document">
        <div class="modal-content team-content">
            <div class="modal-header">
                <h1 class="Teamh1"><i class="fa fa-users" aria-hidden="true"></i>
                    Het Team
                </h1>
            </div>
            <%--Modal body--%>
            <div class="modal-body">
                <section class="sectionStuff">
                    <%--Set each team member in a honeycomb--%>
                    <c:forEach var="teamMember" items="${requestScope.team}">
                        <article class="articleContent">
                            <figure class="Teamfigure">
                                <br/><br/>
                                <h2 class="h2Style">${teamMember.firstName} ${teamMember.lastName}</h2>
                                <p class="paragraphStuff">${teamMember.subject}</p>

                                <c:if test="${not empty teamMember.image}">
                                    <c:choose>
                                        <c:when test="${fn:startsWith(teamMember.image, 'http')}">
                                            <img class="teamImg"
                                                 src="${teamMember.image}"
                                                 alt="${teamMember.id}-teamMemberImg"/>
                                        </c:when>
                                        <c:otherwise>
                                            <img class="teamImg"
                                                 src="${pageContext.request.contextPath}/${initParam.teamImagesPath}/${teamMember.image}"
                                                 alt="${teamMember.id}-teamMemberImg"/>
                                        </c:otherwise>
                                    </c:choose>

                                </c:if>

                                <div class="bs-example">
                                    <!-- Button HTML (to Trigger Modal) -->
                                    <a id="${teamMember.id}" type="text" class="buildModal" data-toggle="modal"
                                       data-target="#${teamMember.id}-teamId">
                                        <div class="toModal">Lees meer</div>
                                    </a>
                                </div>
                            </figure>
                        </article>
                    </c:forEach>

                    <svg width="0" height="0">
                        <defs>
                            <clipPath id="hexagono" clipPathUnits="objectBoundingBox">
                                <polygon points=".25 0, .75 0, 1 .5, .75 1, .25 1, 0 .5"></polygon>
                            </clipPath>
                        </defs>
                    </svg>

                    <%-- Create modal for each team member--%>
                    <c:forEach var="tm" items="${requestScope.team}">
                        <!-- Modal HTML -->
                        <div id="${tm.id}-teamId" class="modal fade">
                            <div class="modal-dialog m-0 ">
                                <div class="modal-content team-content">
                                    <div class="modal-header">
                                        <%--Check for abbreviation--%>
                                        <c:choose>
                                            <c:when test="${tm.abbreviation == null}">
                                                <h4 class="modal-title">${tm.firstName} ${tm.lastName}</h4>
                                            </c:when>
                                            <c:otherwise>
                                                <h4 class="modal-title">${tm.firstName} ${tm.lastName}
                                                    (${tm.abbreviation})</h4>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>

                                    <%--Modal content--%>
                                    <div class="modal-body">
                                        <h5>Gegevens</h5>
                                        <p class="paragraphStuff">${tm.firstName} is: ${tm.subject}</p>
                                        <p class="paragraphStuff">E-mail: <a href="mailto:${tm.email}">${tm.email}</a>
                                        </p>
                                        <%-- Check if there is available information in the database--%>
                                        <c:choose>
                                            <c:when test="${empty tm.info}">
                                                <p class="paragraphStuff">Geen verdere informatie beschikbaar
                                                    over ${tm.firstName}.</p>
                                            </c:when>
                                            <c:otherwise>
                                                <p class="paragraphStuff">Meer informatie:<br/>${tm.info}</p>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>

                                    <%-- Close Button --%>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default close-manually"
                                                data-toclose="${tm.id}-teamId">Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </section>
            </div>

            <div class="modal-footer">
                <button type="button" class="close" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
