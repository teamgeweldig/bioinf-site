<%--
  Authors: Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  The JSP for displaying the information links, these links are received from the database through the servlet
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="container">
    <h1 class="linkTitle"><i class="fa fa-info" aria-hidden="true"></i>
        Information
    </h1>
</div>
<hr class="border-2px">
<div class="container">
    <%-- Fills a block with all information links--%>
    <c:forEach items="${requestScope.informationItems}" var="linkObject">
        <div class="inf-top-margin">
            <a class="li" href="${linkObject.address}">
                <div class=inf-top>
                    <c:choose>
                        <c:when test="${fn:startsWith(linkObject.image, 'http')}">
                            <img class="inf-img" src="${linkObject.image}">
                        </c:when>
                        <c:otherwise>
                            <img class="inf-img" src="img/links/${linkObject.image}">
                        </c:otherwise>
                    </c:choose>


                </div>
                <div class="slider inf-bottom">
                    <div class="inf-name">
                            ${linkObject.name}
                    </div>
                </div>
            </a>
        </div>
    </c:forEach>
</div>
