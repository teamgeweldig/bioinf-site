<%--
  Authors Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  JSP for providing the layout and blocks for the student page with their correct content (another JSP page as an include)
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="no-scroll" id="studentBlocks">
    <%-- Create grid containing all student blocks--%>
    <div class="grid blank-background">
        <div class="grid-sizer"></div>
        <%-- Fact of the day block--%>
        <div class="grid-item grid-item--height-student-blocks grid-item--width4-student-blocks bg-green">
            <jsp:include page="factOfTheDay.jsp"/>
        </div>
        <%-- News block--%>
        <div class="grid-item grid-item--height2-student-blocks grid-item--width3-student-blocks scroll-y">
            <jsp:include page="news.jsp"/>
        </div>
        <%-- Agenda block--%>
        <div class="grid-item grid-item--height3-student-blocks scroll-y">
            <jsp:include page="agenda.jsp"/>
        </div>
        <%-- Links block--%>
        <div class="grid-item grid-item--height2-student-blocks grid-item--width3-student-blocks scroll-y">
            <c:set value="1" var="linkType" scope="request"/>
            <jsp:include page="links.jsp"/>
        </div>
        <%-- Information block--%>
        <div class="grid-item grid-item--height-student-blocks scroll-y">
            <c:set value="2" var="linkType" scope="request"/>
            <jsp:include page="information.jsp"/>
        </div>
        <%-- Courses, room schedules and computer status block--%>
        <div class="grid-item grid-item--height-student-blocks grid-item--width4-student-blocks">
            <jsp:include page="redirectBlocks.jsp"/>
        </div>
        <%--Team block--%>
        <div class="grid-item grid-item--height-student-blocks grid-item--width2-student-blocks add-bg-image">
            <div class="d-flex align-items-center justify-content-center height-100 hovereffect">
                <h2 class="text-center">Het Team</h2>
                <input type="hidden" name="img"
                       value="https://images.pexels.com/photos/296881/pexels-photo-296881.jpeg?w=1260&h=750&auto=compress&cs=tinysrgb">
                <div class="overlay d-flex align-items-center justify-content-center">
                    <p>
                        <a href="" data-toggle="modal" data-target="#teamModal">Meer informatie</a>
                    </p>
                </div>
            </div>
        </div>
        <%--Feedback block--%>
        <div class="grid-item grid-item--height-student-blocks grid-item--width2-student-blocks add-bg-image">
            <div class="d-flex align-items-center justify-content-center height-100 hovereffect">
                <h2 class="text-center">Heeft u tips of opmerkingen over de site?</h2>
                <input type="hidden" name="img"
                       value="https://images.pexels.com/photos/263532/pexels-photo-263532.jpeg">
                <div class="overlay d-flex align-items-center justify-content-center">
                    <p>
                        <a href="" data-toggle="modal" data-target="#feedbackModal">Meer informatie</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="/includes/feedback.jsp"/>
<jsp:include page="team.jsp"/>

