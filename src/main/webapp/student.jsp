<%--
  Authors Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  Top layer JSP for calling all sub JSPs which are required for the student page.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<jsp:include page="includes/header.jsp"/>

<%--Feedback form submitted succes message--%>
<c:if test="${requestScope.successMsg != null}">
    <div class="alert alert-success" id="success-alert">
        <strong>Success!</strong> ${requestScope.successMsg}
    </div>
</c:if>

<div class="d-flex justify-content-center mt-4">
    <jsp:include page="includes/studentBlocks.jsp"/>
</div>

<a href="" class="cd-top">Top</a>

<jsp:include page="includes/footer.jsp"/>
