$(document).ready(function() {

    // Function for limiting the preview to a certain amount of chars
    $(".shortContent").each(function(){
        $(this).html($(this).html().substr(0,83)+"...");
    });

    // Add a image to each div which contains this class
    $('.bg-image').each(function () {
        var imgSrc = $(this).find("input").val();
        $(this).css("background-image", 'url(' + imgSrc + ")");
        $(this).css("background-size", 'contain');
        $(this).css("background-repeat", 'no-repeat');
        $(this).css("background-position", 'center');
    });

});