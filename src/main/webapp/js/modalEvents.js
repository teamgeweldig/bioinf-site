$(document).ready(function () {

    // Close the correct modal when the close button is clicked
    $('.close-manually').click(function () {
        var element = $(this).data('toclose');
        $("#" + element).modal("hide");
    });


    $("#success-alert").fadeTo(5000, 500).slideUp(500, function () {
        $("#success-alert").slideUp(500);
    });

});