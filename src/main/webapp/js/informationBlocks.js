// external js: masonry.pkgd.js

$(document).ready(function () {

    // Set the grid
    $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true,
        gutter: 40
    });


    // Add a background image to div's which contain the class add-bg-image
    $('.add-bg-image').each(function () {
        var imgSrc = $(this).find("input").val();
        $(this).css("background-image", 'linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(' + imgSrc + ")");
        $(this).css("background-size", 'cover');
        $(this).css("background-repeat", 'no-repeat');
        $(this).css("background-position", 'center');
        $(this).css("padding", '0');
    });

    // For each subject, add process the description if available and add these in the right place
    $(".vakken-ul").each(function () {
        var vakken = $(this).html();
        vakken = vakken.split(",");

        if ($(this).siblings("input").val()) {
            var vakDesc = JSON.parse($(this).siblings("input").val());
        } else {
            vakDesc = "";
        }

        $(this).html("");

        var ulElement = $(this);

        $('<li class="vak active">' + "<a href='' class='subj-link' id='main'>Inleiding</a></li>")
            .appendTo(ulElement);

        $.each(vakken, function (i) {
            $('<li class="vak">'+"<a href='' class='subj-link' id="+vakken[i].replace(" ", "")+">"+vakken[i]+"</a></li>")
                .appendTo(ulElement);
            if (vakDesc[vakken[i]]) {
                $("<p class='desc d-none' id='" + vakken[i].replace(" ", "") + "'>" + vakDesc[vakken[i]] + "</p>").appendTo(ulElement.parent());
            }

        });

    });

    // If the user clicks on a subject, display the correct information
    $(".subj-link").click(function (event) {
        event.preventDefault();
        var id = $(this).attr('id');
        var ulElement = $(this).parent().parent();

        $(this).parent().siblings().removeClass('active');
        $(this).parent().addClass('active');

        ulElement.siblings("p").each(function () {
            $(this).addClass('d-none');
        });

        if (id === "main") {
            ulElement.siblings("p.description").removeClass("d-none").addClass('active');
        } else {
            ulElement.siblings("p#" + id).removeClass("d-none").addClass('active');
        }

    });

});

