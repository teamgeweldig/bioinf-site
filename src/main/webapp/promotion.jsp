<%--
  Authors: Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma

  Top layer JSP for calling all sub JSPs to create the promotion page
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<jsp:include page="includes/header.jsp"/>
<br/>

<h1 class="display-1 text-center">Welkom bij Bio-informatica!</h1>
<br/>

<jsp:include page="includes/upperPromotionBlocks.jsp"/>
<br/>

<jsp:include page="includes/slideshow.jsp">
    <jsp:param name="slideshowItems" value="slideshowItems1" />
</jsp:include>
<br/>

<jsp:include page="includes/promotionBlocks.jsp"/>

<jsp:include page="includes/slideshow.jsp">
    <jsp:param name="slideshowItems" value="slideshowItems2" />
</jsp:include>
<br/>

<jsp:include page="includes/coursesBlocks.jsp"/>
<jsp:include page="includes/contact.jsp"/>

<a href="" class="cd-top">Top</a>

<jsp:include page="includes/footer.jsp"/>
