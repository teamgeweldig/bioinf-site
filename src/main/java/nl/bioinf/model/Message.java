/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.model;

import java.util.Date;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

/**
 * Class for storing the information of a message/news object
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class Message {
    private String content;
    private String title;
    private int id;
    private String image;
    private Date lastModified;
    private String author;
    private String source;

    /**
     * Constructor of the message object
     *
     * @param id           unique numeric id
     * @param content      the text of the message
     * @param lastModified the date of alteration
     * @param author       the author
     * @param title        the title of the message
     * @param image        the image to display
     */
    public Message(int id, String content, Date lastModified, String author, String title, String image) {
        this.author = escapeHtml4(author);
        this.content = escapeHtml4(content);
        this.id = id;
        this.lastModified = lastModified;
        this.title = escapeHtml4(title);
        this.image = image;
    }

    /**
     * Alternative constructor for the message object
     * @param id unique numeric id
     * @param content the text of the message
     * @param lastModified the date of alteration
     * @param author the author
     */
    public Message(int id, String content, Date lastModified, String author) {
        this.author = escapeHtml4(author);
        this.content = escapeHtml4(content);
        this.id = id;
        this.lastModified = lastModified;
    }

    /**
     * Alternative constructor for the message object
     * @param id unique numeric id
     * @param content the text of the message
     * @param title the title of the message
     * @param image the image to display
     * @param source the source of the message
     */
    public Message(int id, String title, String content, String image, String source) {
        this.content = escapeHtml4(content);
        this.id = id;
        this.title = escapeHtml4(title);
        this.image = image;
        this.source = source;
    }

    /***************
     *   GETTERS   *
     ***************/
    public String getSource() { return source;}
    public String getImage() {
        return image;
    }
    public String getTitle() {
        return title;
    }
    public String getAuthor() {
        return author;
    }
    public String getContent() {
        return content;
    }
    public int getId() {
        return id;
    }
    public Date getLastModified() {
        return lastModified;
    }

    /***************
     *   SETTERS   *
     ***************/
    public void setTitle(String title) {
        this.title = title;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }
}