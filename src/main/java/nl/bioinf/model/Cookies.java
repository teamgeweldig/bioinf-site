/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.model;

import javax.servlet.http.Cookie;

/**
 * Class for handling cookies
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class Cookies {

    /**
     * Set or change a cookie with the specified variables
     *
     * @param name    name of the cookie
     * @param value   value of the cookie
     * @param cookies a list of all the current cookies
     * @return the new cookie
     */
    public static Cookie setCookie(String name, String value, Cookie[] cookies) {
        Cookie cookie = Cookies.getCookie("student", cookies);
        if (cookie != null) {
            cookie.setValue(value);
        } else {
            cookie = new Cookie(name, value);
            cookie.setMaxAge(60 * 60 * 24 * 30); //Store cookie for 1 year
        }
        return cookie;
    }

    /**
     * Check if a cookie is set
     *
     * @param name    the name of the cookie
     * @param cookies a list of all the current cookies
     * @return boolean indicating if the cookie is set
     */
    public static boolean isSet(String name, Cookie[] cookies) {

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Return a cookie based on it's name
     *
     * @param name    the name of the cookie
     * @param cookies a list of all current cookies
     * @return the found cookie or null if not found
     */
    public static Cookie getCookie(String name, Cookie[] cookies) {
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name)) {
                    return cookie;
                }
            }
        }
        return null;
    }
}
