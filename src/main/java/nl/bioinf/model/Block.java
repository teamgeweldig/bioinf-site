/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.model;

import java.util.Date;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

/**
 * Class to create Block objects, used to store information from the database.
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class Block {
    private int id;
    private String title;
    private String subtitle;
    private final String content;
    private String imageFileName;
    private String link;
    private Short promotionBlockId;
    private Date lastModified;
    private String width;
    private String height;

    /**
     * Constructor of Block object
     *
     * @param id               unique numeric id
     * @param title            name of the block item
     * @param subtitle         sub-name of the block item
     * @param content          information that will be stored in the blocks or in a modal
     * @param imageFileName    name of the image
     * @param link             address to a webpage or location on this website
     * @param promotionBlockId an non-unique id used to group certain blocks together
     * @param lastModified     time the item is last modified in the database
     */
    public Block(int id, String title, String subtitle, String content, String imageFileName, String link,
                 Short promotionBlockId, Date lastModified) {
        this.id = id;
        this.title = escapeHtml4(title);
        this.subtitle = escapeHtml4(subtitle);
        this.content = content;
        this.imageFileName = imageFileName;
        this.link = link;
        this.promotionBlockId = promotionBlockId;
        this.lastModified = lastModified;
    }

    /**
     * Constructor of Block object (with width and height)
     *
     * @param id               unique numeric id
     * @param title            name of the block item
     * @param subtitle         sub-name of the block item
     * @param content          information that will be stored in the blocks or in a modal
     * @param imageFileName    name of the image
     * @param link             address to a webpage or location on this website
     * @param promotionBlockId an non-unique id used to group certain blocks together
     * @param lastModified     time the item is last modified in the database
     * @param width             width of block
     * @param height            height of block
     */
    public Block(int id, String title, String subtitle, String content, String imageFileName, String link,
                 Short promotionBlockId, Date lastModified, String width, String height) {
        this.id = id;
        this.title = escapeHtml4(title);
        this.subtitle = escapeHtml4(subtitle);
        this.content = content;
        this.imageFileName = imageFileName;
        this.link = link;
        this.promotionBlockId = promotionBlockId;
        this.lastModified = lastModified;
        this.width = width;
        this.height = height;
    }

    /***************
     *   GETTERS   *
     ***************/
    public int getId() {
        return id;
    }
    public void setId(int id) { this.id = id; }
    public String getContent() { return content; }
    public String getWidth() {
        return width;
    }
    public String getHeight() {
        return height;
    }
    public String getTitle() {
        return title;
    }
    public String getSubtitle() { return subtitle; }
    public String getImageFileName() { return imageFileName; }
    public Date getLastModified() { return lastModified; }
    public String getLink() {
        return link;
    }
    public Short getPromotionBlockId() { return promotionBlockId; }
    public void setLink(String link) { this.link = link; }

    /***************
     *   SETTERS   *
     ***************/
    public void setImageFileName(String imageFileName) {
        this.imageFileName = imageFileName;
    }
    public void setPromotionBlockId(Short promotionBlockId) { this.promotionBlockId = promotionBlockId; }
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }
    public void setLastModified(Date lastModified) { this.lastModified = lastModified; }
    public void setTitle(String title) { this.title = title; }
}
