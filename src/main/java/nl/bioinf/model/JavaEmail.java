/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.model;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * The JavaEmail class is used to connect to the GMail mail server, to create an email message from the
 * feedback form contents and to send the email to feedback.bioinf.nl@gmail.com.
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class JavaEmail {
    private Properties emailProperties;
    private Session mailSession;
    private MimeMessage emailMessage;


    /**
     * setMailServerProperties() is a method to set the email properties for the GMail mail server connection.
     */
    public void setMailServerProperties() {
        emailProperties = System.getProperties();
        String emailPort = "587";
        emailProperties.put("mail.smtp.port", emailPort);
        emailProperties.put("mail.smtp.auth", "true");
        emailProperties.put("mail.smtp.starttls.enable", "true");
    }


    /**
     * createEmailMessage() creates an email from the feedback form content.
     *
     * @param emailSubject          : the email subject
     * @param emailBody             : the email content
     * @throws AddressException     : if the address is invalid
     * @throws MessagingException   : if a messaging error occurs
     */
    public void createEmailMessage(String emailSubject, String emailBody)
            throws MessagingException {
        mailSession = Session.getDefaultInstance(emailProperties, null);
        emailMessage = new MimeMessage(mailSession);
            emailMessage.addRecipient(Message.RecipientType.TO,
                    new InternetAddress("feedback.bioinf.nl@gmail.com"));

        emailMessage.setSubject(emailSubject);
        emailMessage.setContent(emailBody, "text/html");// for a html email
    }

    /**
     * sendEmail() sends the created email to the recipient (feedback.bioinf.nl@gmail.com).
     *
     * @throws AddressException     : if the address is invalid
     * @throws MessagingException   : if a messaging error occurs
     */
    public void sendEmail() throws MessagingException {
        Transport transport = mailSession.getTransport("smtp");
        String fromUserEmailPassword = "teamGeweldig";
        String fromUser = "feedback.bioinf.nl@gmail.com";
        String emailHost = "smtp.gmail.com";
        transport.connect(emailHost, fromUser, fromUserEmailPassword);
        transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
        transport.close();
    }

}