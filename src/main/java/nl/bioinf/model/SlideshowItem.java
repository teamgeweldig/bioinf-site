/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.model;

import java.util.Date;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

/**
 * Class to create Slideshow objects to store slideshow items from the database
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class SlideshowItem {
    private int id;
    private String title;
    private String subtitle;
    private String imageFileName;
    private String link;
    private Short priority;
    private Short slideshowId;
    private Date lastModified;

    /**
     * Constructor for the SlideshowItem object
     *
     * @param id            unique numeric id
     * @param title         the title of the slideshow item
     * @param subtitle      the subtitle of the slideshow item
     * @param imageFileName name of the image
     * @param link          link address to a web page or position on this website
     * @param priority      needed to order on for the database
     * @param slideshowId   a non-unique id used to group certain items together
     * @param lastModified  the time the object was last modified from the database
     */
    public SlideshowItem(int id, String title, String subtitle, String imageFileName, String link, short priority,
                         short slideshowId, Date lastModified) {
        this.id = id;
        this.title = escapeHtml4(title);
        this.subtitle = escapeHtml4(subtitle);
        this.imageFileName = imageFileName;
        this.link = link;
        this.priority = priority;
        this.slideshowId = slideshowId;
        this.lastModified = lastModified;
    }

    /***************
     *   GETTERS   *
     ***************/
    public int getId() { return id; }
    public String getTitle() {  return title; }
    public Date getLastModified() {
        return lastModified;
    }
    public String getSubtitle() { return subtitle; }
    public Short getSlideshowId() {
        return slideshowId;
    }
    public String getImageFileName() { return imageFileName; }
    public String getLink() { return link; }
    public short getPriority() { return priority; }

    /***************
     *   SETTERS   *
     ***************/
    public void setId(int id) { this.id = id; }
    public void setLink(String link) {
        this.link = link;
    }
    public void setPriority(short priority) { this.priority = priority; }
    public void setImageFileName(String imageFileName) {
        this.imageFileName = imageFileName;
    }
    public void setSlideshowId(Short slideshowId) { this.slideshowId = slideshowId; }
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setLastModified(Date lastModified) { this.lastModified = lastModified; }
}