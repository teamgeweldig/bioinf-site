/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.model;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

/**
 * Class to create Course objects, used to store Course items from the database in
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class Course {
    private int id;
    private String title;
    private String subjects;
    private String description;
    private String image;
    private final String subjectDescriptions;

    /**
     * The constructor for the Course object
     *
     * @param id                  unique numeric id
     * @param title               name of the course
     * @param subjects            all the names of the different subjects
     * @param description         says something about the theme of the course
     * @param image               the image to display
     * @param subjectDescriptions a description about one of the subjects
     **/
    public Course(int id, String title, String subjects, String description, String image, String subjectDescriptions) {
        this.id = id;
        this.title = escapeHtml4(title);
        this.subjects = escapeHtml4(subjects);
        this.description = escapeHtml4(description);
        this.image = image;
        this.subjectDescriptions = escapeHtml4(subjectDescriptions.replace("\'", "\""));
    }

    /***************
     *   GETTERS   *
     ***************/
    public int getId() {
        return id;
    }
    public String getTitle() {
        return title;
    }
    public String getSubjectDescriptions() {
        return this.subjectDescriptions;
    }
    public String getSubjects() {
        return subjects;
    }
    public String getImage() {
        return image;
    }
    public String getDescription() {
        return description;
    }

    /***************
     *   SETTERS   *
     ***************/
    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setTitle(String title) {
        this.title = title;
    }
}
