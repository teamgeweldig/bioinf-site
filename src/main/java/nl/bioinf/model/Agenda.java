/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.model;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

/**
 * Class to create Agenda objects, used to store agenda items from the database
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class Agenda {
    private int id;
    private String title;
    private String date;
    private String content;
    private String location;
    private String image;

    /**
     * The constructor for the Agenda object
     *
     * @param id       unique numeric id
     * @param title    the name of the event
     * @param date     the date of the event
     * @param content  a description about the event
     * @param location the location of the event
     * @param image    the image to display
     */
    public Agenda(int id, String title, String date, String content, String location, String image) {
        this.id = id;
        this.title = escapeHtml4(title);
        this.date = date;
        this.content = escapeHtml4(content);
        this.location = escapeHtml4(location);
        this.image = image;
    }

    /***************
     *   GETTERS   *
     ***************/
    public int getId() { return id; }
    public String getDate() {
        return date;
    }
    public String getTitle() {
        return title;
    }
    public String getContent() {
        return content;
    }
    public String getLocation() {
        return location;
    }
    public String getImage() {
        return image;
    }

    /***************
     *   SETTERS   *
     ***************/
    public void setId(int id) {
        this.id = id;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public void setContent(String description) {
        this.content = description;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public void setImage(String image) {
        this.image = image;
    }
}