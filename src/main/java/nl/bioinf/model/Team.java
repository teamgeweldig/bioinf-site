/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.model;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

/**
 * Class to create Team objects, used to store team items from the database.
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class Team {
    private int id;
    private String firstName;
    private String lastName;
    private String subject;
    private String image;
    private String email;
    private String abbreviation;
    private String room;
    private String info;

    /**
     * @param id           unique numeric id
     * @param firstName    first name of the team member
     * @param lastName     last name of the team member
     * @param subject      what his/her profession within the team is
     * @param image        a picture of the team member
     * @param email        email information of the team member
     * @param abbreviation the four letter code of the team member
     * @param room         his/her own room
     * @param info         more information about the team member
     */
    public Team(int id, String firstName, String lastName, String subject, String image,
                String email, String abbreviation, String room, String info) {
        this.id = id;
        this.firstName = escapeHtml4(firstName);
        this.lastName = escapeHtml4(lastName);
        this.subject = escapeHtml4(subject);
        this.image = image;
        this.email = email;
        this.abbreviation = abbreviation;
        this.room = room;
        this.info = escapeHtml4(info);
    }

    /***************
     *   GETTERS   *
     ***************/
    public int getId() { return id; }
    public String getFirstName() { return firstName; }
    public String getLastName() { return lastName; }
    public String getSubject() { return subject; }
    public String getImage() { return image; }
    public String getEmail() { return email; }
    public String getAbbreviation() { return abbreviation; }
    public String getRoom() { return room; }
    public String getInfo() { return info; }

    /***************
     *   SETTERS   *
     ***************/
    public void setId(int id) { this.id = id; }
    public void setFirstName(String firstName) { this.firstName = firstName; }
    public void setLastName(String lastName) { this.lastName = lastName; }
    public void setSubject(String subject) { this.subject = subject; }
    public void setImage(String image) { this.image = image; }
    public void setEmail(String email) { this.email = email; }
    public void setAbbreviation(String abbreviation) { this.abbreviation = abbreviation; }
    public void setRoom(String room) { this.room = room; }
    public void setInfo(String info) { this.info = info; }
}