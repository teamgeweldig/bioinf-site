/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.model;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

/**
 * Class to create Link and Information objects, used to store link items or information items from the database
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class Link {
    private int id;
    private int type;
    private String name;
    private String address;
    private String image;

    /**
     * Constructor of the link object
     *
     * @param id      unique numeric id
     * @param type    id to distinguish link from information
     * @param name    the name of the link/place it refers to
     * @param address the web address
     * @param image   the image to display
     */
    public Link(int id, int type, String name, String address, String image) {
        this.id = id;
        this.type = type;
        this.name = escapeHtml4(name);
        this.address = address;
        this.image = image;
    }

    /***************
     *   GETTERS   *
     ***************/
    public int getId() {
        return id;
    }
    public int getType() {
        return type;
    }
    public String getImage() {
        return image;
    }
    public String getName() {
        return name;
    }
    public String getAddress() {
        return address;
    }

    /***************
     *   SETTERS   *
     ***************/
    public void setName(String name) {
        this.name = name;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setType(int type) {
        this.type = type;
    }
    public void setImage(String image) {
        this.image = image;
    }
}