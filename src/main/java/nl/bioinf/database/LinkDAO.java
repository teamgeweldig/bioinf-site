/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.Link;

import java.util.List;

/**
 * Interface for requesting link items and information items from the database
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public interface LinkDAO {

    /**
     * Used to request all links items from the database
     * @return list of all link objects
     */
    List<Link> getAllLinks();

    /**
     * Used to request all information items from the database
     * @return list of all information objects
     */
    List<Link> getAllInformation();
}
