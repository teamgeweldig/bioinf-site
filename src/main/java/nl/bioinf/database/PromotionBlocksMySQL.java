/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.Block;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Class to request promotion block material from the database
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class PromotionBlocksMySQL implements BlocksDAO {

    private Connection connection;
    private PreparedStatement slideshowItem;
    private PreparedStatement allSlideshowItems;
    private static PromotionBlocksMySQL uniqueInstance;
    private List<Block> promotionBlocks;
    private final Calendar date;

    /**
     * Private constructor to use the singleton principle
     *
     * @param connection Connection object which is connected to the database
     */
    private PromotionBlocksMySQL(Connection connection) {
        date = Calendar.getInstance();
        date.setTime(new Date());
        this.connection = connection;
    }

    /**
     * Method to implement the singleton principle and allow only one instance of this object
     *
     * @param connection Connection object which is connected to the database
     * @return A LinkMySQL instance which is unique
     */
    public static PromotionBlocksMySQL getInstance(Connection connection) {
        if (uniqueInstance == null) {
            uniqueInstance = new PromotionBlocksMySQL(connection);
        } else {
            try {
                if (!uniqueInstance.connection.isValid(10)) {
                    uniqueInstance.connection = connection;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return uniqueInstance;
    }

    /**
     * Function to receive a promotion block with a specified id from the database
     *
     * @param id used to get a item from the database with the same id
     * @return a promotion block object
     */
    @Override
    public Block getPromotionBlock(int id) {
        try {
            if (slideshowItem == null) {
                String query = "SELECT * FROM promotionBlocks WHERE id = ?";
                slideshowItem = connection.prepareStatement(query);
            }
            slideshowItem.setString(1, Integer.toString(id));
            ResultSet result = slideshowItem.executeQuery();
            if (result.next()) {
                return new Block(result.getInt(1), result.getString(2),
                        result.getString(3), result.getString(4),
                        result.getString(5),
                        result.getString(6), result.getShort(7),
                        result.getDate(8));
            }
            slideshowItem.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void insertPromotionBlock(int id, String content) {
        // To implement...
    }

    /**
     * Function to receive a limited amount of promotion blocks with a specified slideshow id from the database,
     * this also gets ordered by date
     *
     * @param limit            used to give a limit on how many items you want from the database
     * @param promotionBlockId used to get a group of items with the same id
     * @return a list containing a limited amount of promotion block objects
     */
    @Override
    public List<Block> getLimitedPromotionBlocks(int limit, int promotionBlockId) {

        try {
            Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(new Date());
            if (promotionBlocks == null || date.get(Calendar.DAY_OF_YEAR) != currentDate.get(Calendar.DAY_OF_YEAR)) {
                date.setTime(new Date());
                promotionBlocks = new ArrayList<>();
                if (allSlideshowItems == null) {
                    String query = "SELECT * FROM promotionBlocks WHERE promotionBlockId = ? ORDER BY date LIMIT ?";
                    allSlideshowItems = connection.prepareStatement(query);
                }
                allSlideshowItems.setInt(1, promotionBlockId);
                allSlideshowItems.setInt(2, limit);
                ResultSet result = allSlideshowItems.executeQuery();
                while (result.next()) {
                    Block promotionBlock = new Block(result.getInt(1),
                            result.getString(2),
                            result.getString(3),
                            result.getString(4),
                            result.getString(5),
                            result.getString(5),
                            result.getShort(7),
                            result.getDate(8));
                    promotionBlocks.add(promotionBlock);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return promotionBlocks;
    }

    /**
     * Function to receive all information items from the database where type is equal to 2
     * @return a list containing all promotion block objects
     */
    @Override
    public List<Block> getAllPromotionBlocks() {
        try {
            Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(new Date());
            if (promotionBlocks == null || date.get(Calendar.DAY_OF_YEAR) != currentDate.get(Calendar.DAY_OF_YEAR)) {
                date.setTime(new Date());
                promotionBlocks = new ArrayList<>();
                String query = "SELECT promotionBlocks.*, blockInfo.width, blockInfo.height FROM promotionBlocks, blockInfo WHERE promotionBlocks.promotionBlockId = blockInfo.id ORDER BY promotionBlockId, date LIMIT 10";
                allSlideshowItems = connection.prepareStatement(query);

                ResultSet result = allSlideshowItems.executeQuery();
                while (result.next()) {
                    Block promotionBlock = new Block(result.getInt(1),
                            result.getString(2),
                            result.getString(3),
                            result.getString(4),
                            result.getString(5),
                            result.getString(6),
                            result.getShort(7),
                            result.getDate(8),
                            result.getString(9),
                            result.getString(10));
                    promotionBlocks.add(promotionBlock);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return promotionBlocks;
    }

}
