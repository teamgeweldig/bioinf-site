/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.SlideshowItem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Class to request slideshow items from the database
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class SlideshowItemsMySQL implements SlideshowItemsDAO {

    public Connection connection;
    private PreparedStatement slideshowItem;
    private static SlideshowItemsMySQL uniqueInstance;
    private final Calendar date;
    private List<SlideshowItem> slideshowItems;

    /**
     * Private constructor to use the singleton principle
     *
     * @param connection Connection object which is connected to the database
     */
    private SlideshowItemsMySQL(Connection connection) {
        date = Calendar.getInstance();
        date.setTime(new Date());
        this.connection = connection;
    }

    /**
     * Method to implement the singleton principle and allow only one instance of this object
     *
     * @param connection Connection object which is connected to the database
     * @return A LinkMySQL instance which is unique
     */
    public static SlideshowItemsMySQL getInstance(Connection connection) {
        if (uniqueInstance == null) {
            uniqueInstance = new SlideshowItemsMySQL(connection);
        } else {
            if (!uniqueInstance.checkConnection()) {
                uniqueInstance.connection = connection;
            }
        }
        return uniqueInstance;
    }

    /**
     * Checks if the connection with the database is valid
     *
     * @return true if the connection is valid; false if it isn't
     */
    public boolean checkConnection() {
        try {
            return connection.isValid(10);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Function to receive a slideshow item with a specified id from the database
     *
     * @param id used to get a item from the database with the same id
     * @return a Slideshow item object
     */
    @Override
    public SlideshowItem getSlideshowItem(int id) {
        try {
            if (slideshowItem == null) {
                String query = "SELECT * FROM slideshowItems WHERE id = ?";
                slideshowItem = connection.prepareStatement(query);
            }
            slideshowItem.setString(1, Integer.toString(id));
            ResultSet result = slideshowItem.executeQuery();
            if (result.next()) {
                return new SlideshowItem(result.getInt(1), result.getString(2),
                        result.getString(3), result.getString(4),
                        result.getString(5), result.getShort(6),
                        result.getShort(7), result.getDate(8));
            }
            slideshowItem.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void insertSlideshowItem(int id, String content) {
        // To implement...
    }

    /**
     * Function to receive a limited amount of slideshow items with a specified slideshow id from the database,
     * this also gets ordered by priority
     *
     * @param limit       used to give a limit on how many items you want from the database
     * @param slideshowId used to get a group of items with the same id
     * @return a list containing a limited amount of Slideshow item objects
     */
    @Override
    public List<SlideshowItem> getLimitedItems(int limit, int slideshowId) {

        try {
            Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(new Date());
            if (slideshowItems == null || date.get(Calendar.DAY_OF_YEAR) != currentDate.get(Calendar.DAY_OF_YEAR)) {
                slideshowItems = new ArrayList<>();
                date.setTime(new Date());

                String query = "SELECT * FROM slideshowItems WHERE slideshowId = ? ORDER BY -priority DESC, date LIMIT ?";
                PreparedStatement allSlideshowItems = connection.prepareStatement(query);

                allSlideshowItems.setInt(1, slideshowId);
                allSlideshowItems.setInt(2, limit);
                ResultSet result = allSlideshowItems.executeQuery();
                while (result.next()) {
                    SlideshowItem slideshowItem = new SlideshowItem(result.getInt(1),
                            result.getString(2),
                            result.getString(3),
                            result.getString(4),
                            result.getString(5),
                            result.getShort(6),
                            result.getShort(7),
                            result.getDate(8));
                    slideshowItems.add(slideshowItem);
                }

                allSlideshowItems.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return slideshowItems;
    }

}
