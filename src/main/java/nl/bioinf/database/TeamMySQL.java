/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.Team;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Class for requesting team members from the DB
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class TeamMySQL implements TeamDAO {

    private Connection connection;
    private PreparedStatement teamWithID;
    private static TeamMySQL uniqueInstance;
    private List<Team> members;
    private final Calendar date;


    /**
     * Private constructor to use the singleton principle
     *
     * @param connection Connection object which is connected to the database
     */
    private TeamMySQL(Connection connection) {
        date = Calendar.getInstance();
        date.setTime(new Date());
        this.connection = connection;
    }

    /**
     * Method to implement the singleton principle and allow only one instance of this object
     *
     * @param connection Connection object which is connected to the database
     * @return A TeamMySQL instance which is unique
     */
    public static TeamMySQL getInstance(Connection connection) {
        if (uniqueInstance == null) {
            uniqueInstance = new TeamMySQL(connection);
        } else {
            try {
                if (!uniqueInstance.connection.isValid(10)) {
                    uniqueInstance.connection = connection;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return uniqueInstance;
    }

    /**
     * Method responsible for getting a specific teamMember based on its ID
     *
     * @param id The unique ID of the team member (corresponds to the PK in the DB)
     * @return The resulting Team object
     */
    @Override
    public Team getTeamMember(int id) {
        try {
            if (teamWithID == null) {
                String query = "SELECT * FROM team WHERE id = ?";
                teamWithID = connection.prepareStatement(query);
            }
            teamWithID.setString(1, Integer.toString(id));
            ResultSet result = teamWithID.executeQuery();
            if (result.next()) {
                return new Team(result.getInt(1),
                        result.getString(2),
                        result.getString(3),
                        result.getString(4),
                        result.getString(5),
                        result.getString(6),
                        result.getString(7),
                        result.getString(8),
                        result.getString(9));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Used for inserting a new team member in the DB
     *
     * @param firstName The first name of the team member
     */
    @Override
    public void insertTeam(String firstName) {
        // To implement...
    }

    /**
     * Used for getting all team members up to a specific limit
     *
     * @return An array list containing a Team object for each team member
     */
    @Override
    public List<Team> getTeamMembers() {
        try {
            Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(new Date());
            if (members == null || date.get(Calendar.DAY_OF_YEAR) != currentDate.get(Calendar.DAY_OF_YEAR)) {
                members = new ArrayList<>();
                date.setTime(new Date());
                String query = "SELECT * FROM team";
                PreparedStatement allTeam = connection.prepareStatement(query);


                ResultSet result = allTeam.executeQuery();
                while (result.next()) {
                    Team member = new Team(result.getInt(1),
                            result.getString(2),
                            result.getString(3),
                            result.getString(4),
                            result.getString(5),
                            result.getString(6),
                            result.getString(7),
                            result.getString(8),
                            result.getString(9));
                    members.add(member);
                }
                allTeam.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return members;
    }

    /**
     * A default method for requesting 'all' team members(capped at 100 items for efficiency)
     *
     * @return An array list containing a Team object for each team member
     */
    @Override
    public List<Team> getAllTeamMembers() {
        return getTeamMembers();
    }

}