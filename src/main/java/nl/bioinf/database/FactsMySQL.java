/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.Message;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

/**
 * Database access class for getting facts from the database
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class FactsMySQL implements FactsDAO {

    private Connection connection;
    private static FactsMySQL uniqueInstance;
    private final Calendar date;
    private Message fact;

    /**
     * Private constructor to use the singleton principle
     *
     * @param connection Connection object which is connected to the database
     */
    private FactsMySQL(Connection connection) {
        date = Calendar.getInstance();
        date.setTime(new Date());
        this.connection = connection;
    }

    /**
     * Method to implement the singleton principle and allow only one instance of this object
     *
     * @param connection Connection object which is connected to the database
     * @return A FactsMySQL instance which is unique
     */
    public static FactsMySQL getInstance(Connection connection) {
        if (uniqueInstance == null) {
            uniqueInstance = new FactsMySQL(connection);
        } else {
            try {
                if (!uniqueInstance.connection.isValid(10)) {
                    uniqueInstance.connection = connection;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return uniqueInstance;
    }

    /**
     * Function which retrieves a random fact based on a random ID
     *
     * @return A object containing all information about the fact
     */
    @Override
    public Message getRandomFact() {
        try {
            Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(new Date());
            if (fact == null || date.get(Calendar.DAY_OF_YEAR) != currentDate.get(Calendar.DAY_OF_YEAR)) {
                date.setTime(new Date());
                String query = "SELECT * FROM facts WHERE id = (SELECT CEIL(RAND()*(SELECT MAX(id) FROM facts)))";
                PreparedStatement randomFact = connection.prepareStatement(query);
                ResultSet result = randomFact.executeQuery();
                if (result.next()) {
                    fact = new Message(result.getInt(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5));
                }
                randomFact.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fact;
    }

}
