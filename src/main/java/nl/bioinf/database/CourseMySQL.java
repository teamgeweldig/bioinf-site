/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.Course;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Class to request Course items from the database
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class CourseMySQL implements CourseDAO {

    private Connection connection;
    private static CourseMySQL uniqueInstance;
    private List<Course> courseItems;
    private final Calendar date;

    /**
     * Private constructor to use the singleton principle
     *
     * @param connection Connection object which is connected to the database
     */
    private CourseMySQL(Connection connection) {
        date = Calendar.getInstance();
        date.setTime(new Date());
        this.connection = connection;
    }

    /**
     * Method to implement the singleton principle and allow only one instance of this object
     *
     * @param connection Connection object which is connected to the database
     * @return A AgendaMySQL instance which is unique
     */
    public static CourseMySQL getInstance(Connection connection) {
        if (uniqueInstance == null) {
            uniqueInstance = new CourseMySQL(connection);
        } else {
            try {
                if (!uniqueInstance.connection.isValid(10)) {
                    uniqueInstance.connection = connection;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return uniqueInstance;
    }

    /**
     * Function for receiving all Course items ordered by id from the database
     *
     * @return a list conaining all Course items from the database
     */
    @Override
    public List<Course> getAllCourseItems() {
        try {
            Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(new Date());
            if (courseItems == null || date.get(Calendar.DAY_OF_YEAR) != currentDate.get(Calendar.DAY_OF_YEAR)) {
                courseItems = new ArrayList<>();
                date.setTime(new Date());
                String query = "SELECT * FROM courses ORDER BY id LIMIT 13";
                PreparedStatement courseItem = connection.prepareStatement(query);

                ResultSet result = courseItem.executeQuery();
                while (result.next()) {
                    String jsonData = result.getString(6);
                    if (jsonData == null) {
                        jsonData = "";
                    }
                    Course Course = new Course(result.getInt(1), result.getString(2),
                            result.getString(3), result.getString(4), result.getString(5), jsonData
                    );
                    courseItems.add(Course);
                }
                courseItem.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return courseItems;
    }

}
