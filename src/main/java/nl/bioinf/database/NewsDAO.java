/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.Message;

import java.util.List;

/**
 * Interface for requesting news items from the DB
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public interface NewsDAO {

    /**
     * Used for inserting a new news element in the DB
     *
     * @param content The content for the news item
     */
    void insertNews(String content);

    /**
     * Used for getting all news items up to a specific limit
     *
     * @param limit The maximum number of items to get
     * @return An array list containing a Message object for each news item
     */
    List<Message> getLimitedNewsItems(int limit);

    /**
     * Method responsible for getting a specific newsItem based on its ID
     * @param id The unique ID of the news item (corresponds to the PK in the DB)
     * @return The resulting Message object
     */
    Message getNewsItem(int id);

    /**
     * A default method for requesting 'all' news items (capped at 100 items for efficiency)
     * @return An array list containing a Message object for each news item
     */
    List<Message> getAllNewsItems();

}
