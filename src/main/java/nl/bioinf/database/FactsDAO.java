/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.Message;

/**
 * Interface for requesting a fact from the DB
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public interface FactsDAO {

    /**
     * Used for retrieving a random fact from the database
     *
     * @return A message object containing a random fact
     */
    Message getRandomFact();

}
