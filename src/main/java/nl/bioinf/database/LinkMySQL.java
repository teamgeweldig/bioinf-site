/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.Link;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Class to request link and information items from the database
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class LinkMySQL implements LinkDAO {

    private Connection connection;
    private static LinkMySQL uniqueInstance;
    private final Calendar date;
    private List<Link> linkItems;
    private List<Link> informationItems;

    /**
     * Private constructor to use the singleton principle
     *
     * @param connection Connection object which is connected to the database
     */
    private LinkMySQL(Connection connection) {
        date = Calendar.getInstance();
        date.setTime(new Date());
        this.connection = connection;
    }

    /**
     * Method to implement the singleton principle and allow only one instance of this object
     *
     * @param connection Connection object which is connected to the database
     * @return A LinkMySQL instance which is unique
     */
    public static LinkMySQL getInstance(Connection connection) {
        if (uniqueInstance == null) {
            uniqueInstance = new LinkMySQL(connection);
        } else {
            try {
                if (!uniqueInstance.connection.isValid(10)) {
                    uniqueInstance.connection = connection;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return uniqueInstance;
    }

    /**
     * function to receive all link items where type is equal to 1 from the database
     *
     * @return a list containing all link objects
     */
    @Override
    public List<Link> getAllLinks() {
        try {
            Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(new Date());
            if (linkItems == null || date.get(Calendar.DAY_OF_YEAR) != currentDate.get(Calendar.DAY_OF_YEAR)) {
                linkItems = new ArrayList<>();
                date.setTime(new Date());

                String query = "SELECT * FROM links WHERE type = 1";
                PreparedStatement infoQuery = connection.prepareStatement(query);

                ResultSet result = infoQuery.executeQuery();
                while (result.next()) {
                    Link linkItem = new Link(result.getInt(1), result.getInt(2),
                            result.getString(3), result.getString(4), result.getString(5));
                    linkItems.add(linkItem);
                }
                infoQuery.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return linkItems;
    }

    /**
     * Function to receive all information items from the database where type is equal to 2
     * @return a list containing all information objects
     */
    @Override
    public List<Link> getAllInformation() {
        try {
            Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(new Date());
            if (informationItems == null || date.get(Calendar.DAY_OF_YEAR) != currentDate.get(Calendar.DAY_OF_YEAR)) {
                informationItems = new ArrayList<>();
                date.setTime(new Date());

                String query = "SELECT * FROM links WHERE type = 2";
                PreparedStatement infoQuery = connection.prepareStatement(query);

                ResultSet result = infoQuery.executeQuery();
                while (result.next()) {
                    Link informationItem = new Link(result.getInt(1), result.getInt(2),
                            result.getString(3), result.getString(4), result.getString(5));
                    informationItems.add(informationItem);
                }
                infoQuery.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return informationItems;
    }

}
