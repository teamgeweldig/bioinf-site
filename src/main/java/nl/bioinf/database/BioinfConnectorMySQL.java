/**
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.* All rights reserved.
 */
package nl.bioinf.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Common connector class which can be used for all database operations
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class BioinfConnectorMySQL implements DbConnector {

    private Connection connection;
    private final String dbUser;
    private final String dbPass;
    private final String dbUrl;
    private static BioinfConnectorMySQL uniqueInstance;

    /**
     * Private constructor to use the singleton principle
     *
     * @param dbUrl  The url of the database
     * @param dbUser The username to login to the database
     * @param dbPass The password which is needed for the provided username
     */
    private BioinfConnectorMySQL(String dbUrl, String dbUser, String dbPass) {
        this.dbPass = dbPass;
        this.dbUrl = dbUrl;
        this.dbUser = dbUser;
    }

    /**
     * Method to implement the singleton principle and allow only one instance of this object
     *
     * @param dbUrl  The url of the database
     * @param dbUser The username to login to the database
     * @param dbPass The password which is needed for the provided username
     * @return A BioinfConnectorMySQL instance which is unique
     */
    public static BioinfConnectorMySQL getInstance(String dbUrl, String dbUser, String dbPass){
        if (uniqueInstance == null ){
            uniqueInstance = new BioinfConnectorMySQL(dbUrl, dbUser, dbPass);
            uniqueInstance.connect();
        } else {
            try {
                if (!uniqueInstance.getConnection().isValid(10)) {
                    uniqueInstance.connect();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return uniqueInstance;
    }

    /**
     * Function for connecting to the database
     */
    @Override
    public void connect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(dbUrl,dbUser,dbPass);
        } catch (Exception exp) {
            exp.printStackTrace();
        }
    }

    /**
     * Get the connection object for further usage
     * @return A Connection object containing the active connection
     */
    @Override
    public Connection getConnection() {
        return this.connection;
    }

    /**
     * Disconnect the active connection
     */
    public void disconnect() {
        this.connection = null;
    }

}
