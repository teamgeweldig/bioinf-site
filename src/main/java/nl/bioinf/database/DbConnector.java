/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import java.sql.Connection;

/**
 * Interface for creating a Connection to the the database
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public interface DbConnector {

    /**
     * Connect to the database
     */
    void connect();

    /**
     * Get the connection object for further usage
     *
     * @return A Connection object containing the active connection
     */
    Connection getConnection();

    /**
     * Disconnect the active connection
     */
    void disconnect();

}
