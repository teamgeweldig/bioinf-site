/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.Agenda;

import java.util.List;

/**
 * Interface for requesting agenda items from the database
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public interface AgendaDAO {
    /**
     * Request all agenda items from the database.
     * @return List list of all agenda objects
     */
    List <Agenda> getAllAgendaItems();

    /**
     * Request specific agenda item from the database.
     *
     * @param id id of agenda item
     * @return Agenda object with specific ID
     */
    Agenda getAgendaItem(int id);

}