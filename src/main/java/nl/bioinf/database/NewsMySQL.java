/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.Message;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Class for requesting news items from the DB
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class NewsMySQL implements NewsDAO {

    private Connection connection;
    private PreparedStatement newsWithID;
    private static NewsMySQL uniqueInstance;
    private final Calendar date;
    private List<Message> newsItems;

    /**
     * Method to implement the singleton principle and allow only one instance of this object
     *
     * @param connection Connection object which is connected to the database
     * @return A NewsMySQL instance which is unique
     */
    public static NewsMySQL getInstance(Connection connection) {
        if (uniqueInstance == null) {
            uniqueInstance = new NewsMySQL(connection);
        } else {
            try {
                if (!uniqueInstance.connection.isValid(10)) {
                    uniqueInstance.connection = connection;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return uniqueInstance;
    }

    /**
     * Private constructor to use the singleton principle
     *
     * @param connection Connection object which is connected to the database
     */
    private NewsMySQL(Connection connection) {
        date = Calendar.getInstance();
        date.setTime(new Date());
        this.connection = connection;
    }

    /**
     * Method responsible for getting a specific newsItem based on its ID
     *
     * @param id The unique ID of the news item (corresponds to the PK in the DB)
     * @return The resulting Message object
     */
    @Override
    public Message getNewsItem(int id) {
        try {
            if (newsWithID == null) {
                String query = "SELECT * FROM news WHERE id = ?";
                newsWithID = connection.prepareStatement(query);
            }
            newsWithID.setString(1, Integer.toString(id));
            ResultSet result = newsWithID.executeQuery();
            if (result.next()) {
                return new Message(result.getInt(1), result.getString(2), result.getDate(3),
                        result.getString(4), result.getString(5), result.getString(6));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Used for inserting a new news element in the DB
     *
     * @param content The content for the news item
     */
    @Override
    public void insertNews(String content) {
        // To implement...
    }

    /**
     * Used for getting all news items up to a specific limit
     *
     * @param limit The maximum number of items to get
     * @return An array list containing a Message object for each news item
     */
    @Override
    public List<Message> getLimitedNewsItems(int limit) {
        try {
            Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(new Date());
            if (newsItems == null || date.get(Calendar.HOUR_OF_DAY) != currentDate.get(Calendar.HOUR_OF_DAY)) {
                newsItems = new ArrayList<>();
                date.setTime(new Date());
                String query = "SELECT * FROM news ORDER BY date LIMIT ?";
                PreparedStatement allNews = connection.prepareStatement(query);

                allNews.setInt(1, limit);
                ResultSet result = allNews.executeQuery();
                while (result.next()) {
                    Message message = new Message(result.getInt(1), result.getString(2), result.getDate(3),
                            result.getString(4), result.getString(5), result.getString(6));
                    newsItems.add(message);
                }
                allNews.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return newsItems;
    }

    /**
     * A default method for requesting 'all' news items (capped at 100 items for efficiency)
     *
     * @return An array list containing a Message object for each news item
     */
    @Override
    public List<Message> getAllNewsItems() {
        return getLimitedNewsItems(100);
    }

}