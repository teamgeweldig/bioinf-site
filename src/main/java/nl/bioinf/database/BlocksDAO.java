/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.Block;

import java.util.List;

/**
 * Interface for requesting blocks from the database
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public interface BlocksDAO {

    /**
     * Used for retrieving a promotion block based on it's id
     *
     * @param id Unique id of the block
     * @return The block as an object
     */
    Block getPromotionBlock(int id);

    /**
     * Insert a promotion block in the database
     *
     * @param id      The id of the new promotion block
     * @param content The content of the promotion block
     */
    void insertPromotionBlock(int id, String content);

    /**
     * Get a specified number of promotionblocks
     *
     * @param limit The maximum number of blocks to return
     * @param promotionBlockId A specific id specifying which blocks to get
     * @return A list of blocks
     */
    List<Block> getLimitedPromotionBlocks(int limit, int promotionBlockId);

    /**
     * Get all promotion blocks in the database
     *
     * @return A list of all promotion blocks
     */
    List<Block> getAllPromotionBlocks();

}
