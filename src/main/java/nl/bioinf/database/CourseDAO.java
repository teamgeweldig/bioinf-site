/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.Course;

import java.util.List;

/**
 * Interface for requesting Course items from the database
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public interface CourseDAO {

    /**
     * Get all coure items
     *
     * @return A list of course objects
     */
    List<Course> getAllCourseItems();

}
