/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.Agenda;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Class to request agenda items from the database
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public class AgendaMySQL implements AgendaDAO {

    private Connection connection;
    private static AgendaMySQL uniqueInstance;
    private final Calendar date;
    private List<Agenda> agendaItems;

    /**
     * Constructor to implement the singleton principle and allow only one instance of this object
     *
     * @param connection connection object which is connected to the database
     */
    private AgendaMySQL(Connection connection) {
        date = Calendar.getInstance();
        date.setTime(new Date());
        this.connection = connection;
    }

    /**
     * Method to implement the singleton principle and allow only one instance of this object
     *
     * @param connection Connection object which is connected to the database
     * @return A AgendaMySQL instance which is unique
     */
    public static AgendaMySQL getInstance(Connection connection) {
        if (uniqueInstance == null) {
            uniqueInstance = new AgendaMySQL(connection);
        } else {
            try {
                if (!uniqueInstance.connection.isValid(10)) {
                    uniqueInstance.connection = connection;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return uniqueInstance;
    }

    /**
     * Function for receiving all agendaitems ordered by time from the database
     *
     * @return a list conaining all agenda items from the database
     */
    @Override
    public List<Agenda> getAllAgendaItems() {
        try {
            Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(new Date());
            if (agendaItems == null || date.get(Calendar.DAY_OF_YEAR) != currentDate.get(Calendar.DAY_OF_YEAR)) {
                agendaItems = new ArrayList<>();

                String query = "SELECT * FROM agenda ORDER BY DATE";
                PreparedStatement agendaItemWithId = connection.prepareStatement(query);

                ResultSet result = agendaItemWithId.executeQuery();
                while (result.next()) {
                    Agenda agenda = new Agenda(result.getInt(1), result.getString(2),
                            result.getString(3), result.getString(4), result.getString(5),
                            result.getString(6));
                    agendaItems.add(agenda);
                }
                agendaItemWithId.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return agendaItems;
    }

    /**
     * Function for returning an agenda item based on its id
     *
     * @param id the unique id of the item
     * @return An agenda object containing the necessary information
     */
    @Override
    public Agenda getAgendaItem(int id) {
        Agenda item = null;
        try {
            String query = "SELECT * FROM news WHERE id = ?";
            PreparedStatement agendaWithID = connection.prepareStatement(query);
            agendaWithID.setString(1, Integer.toString(id));
            ResultSet result = agendaWithID.executeQuery();
            if (result.next()) {
                item = new Agenda(result.getInt(1), result.getString(2),
                        result.getString(3), result.getString(4), result.getString(5),
                        result.getString(6));
            }
            agendaWithID.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return item;
    }

}
