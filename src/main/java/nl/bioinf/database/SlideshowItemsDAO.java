/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.SlideshowItem;

import java.util.List;

/**
 * Interface for requesting slideshow items from the database
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public interface SlideshowItemsDAO {
    /**
     * Request slideshow item from the database.
     * @param id ID of slideshow item
     * @return Slideshow object with specific ID
     */
    SlideshowItem getSlideshowItem(int id);

    /**
     * Insert new slideshow item
     * @param id id of slideshow
     * @param content content of slideshow object
     */
    void insertSlideshowItem(int id, String content);

    /**
     * Request limited slideshow items from the database.
     *
     * @param limit limit of items
     * @param slideshowId ID of slideshow
     * @return list with all slideshow objects
     */
    List<SlideshowItem> getLimitedItems(int limit, int slideshowId);

}
