/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.database;

import nl.bioinf.model.Team;

import java.util.List;

/**
 * Interface for requesting team members from the DB
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
public interface TeamDAO {

    /**
     * Used for inserting a new team element in the DB
     *
     * @param firstName The first name of the team member
     */
    void insertTeam(String firstName);

    /**
     * Used for getting all team members
     *
     * @return An array list containing a Team object for each team member
     */
    List<Team> getTeamMembers();

    /**
     * Method responsible for getting a specific teamMember based on its ID
     * @param id The unique ID of the team member (corresponds to the PK in the DB)
     * @return The resulting Team object
     */
    Team getTeamMember(int id);

    /**
     * A default method for requesting 'all' team members (capped at 100 items for efficiency)
     * @return An array list containing a Team object for each team member
     */
    List<Team> getAllTeamMembers();

}
