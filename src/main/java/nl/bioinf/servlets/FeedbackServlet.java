/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.servlets;

import nl.bioinf.model.JavaEmail;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The FeedbackServlet class is used to send content to the bioinf.nl feedback page.
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hak   voort and Wietse Reitsma
 * @version 1.0
 */
@WebServlet(name = "FeedbackServlet", urlPatterns = "/feedback")
public class FeedbackServlet extends HttpServlet {

    /**
     * The doPost method handles POST requests.
     *
     * @param request:  Provides client request information for HTTP servlets.
     * @param response: Provides HTTP specific functionality in sending a response.
     * @throws ServletException: If problems are encountered while handling the HttpServletRequest and/or -response,
     *                           a ServletException will be raised.
     * @throws IOException       If an input or output error happens when the servlet handles the GET request, an
     *                           an IOException is thrown.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getParameter("submit") != null) {
            // create new JavaEmail object and set the GMail server properties.
            JavaEmail javaEmail = new JavaEmail();
            javaEmail.setMailServerProperties();

            // Get the email subject and initialise the email body.
            String emailSubject = request.getParameter("subject");
            String emailBody = "";

            // If the name of the user is not null, the email body will add the name of the user.
            if (request.getParameter("name") != null) {
                emailBody = "Afzender: " + request.getParameter("name")
                        + "<br>";
            }

            // Same goes with the user email.
            if (request.getParameter("email") != null) {
                emailBody = emailBody + "Email: "
                        + request.getParameter("email") + "<br>";
            }

            // ... and the subject.
            if (request.getParameter("subject") != null) {
                emailBody = emailBody + "Onderwerp: "
                        + request.getParameter("subject") + "<br>";
            }

            // ... and the email message.
            if (request.getParameter("message") != null) {
                emailBody = emailBody + "<br/>Bericht: <br/>" + request.getParameter("message")
                        + "<br>";
            }

            // try to create an email message from the form contents.
            try {
                javaEmail.createEmailMessage(emailSubject, emailBody);
            } catch (MessagingException e) {
                e.printStackTrace();
            }

            // try to send the email and redirect to the thank you page with the corresponding thank you message.
            try {
                javaEmail.sendEmail();
                response.sendRedirect("student?feedback=success");

            } catch (MessagingException me) {
                me.printStackTrace();
            }
        }
    }

    /**
     * The doGet method handles GET requests.
     *
     * @param request:  Provides client request information for HTTP servlets.
     * @param response: Provides HTTP specific functionality in sending a response.
     * @throws ServletException: If problems are encountered while handling the HttpServletRequest and/or -response,
     *                           a ServletException will be raised.
     * @throws IOException       If an input or output error happens when the servlet handles the GET request, an
     *                           an IOException is thrown.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("feedback.jsp");
        view.forward(request, response);
    }
}
