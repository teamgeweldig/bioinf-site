/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */

package nl.bioinf.servlets;

import nl.bioinf.database.*;
import nl.bioinf.model.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet for the student page, requests all data for the page
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
@WebServlet(name = "StudentServlet", urlPatterns = "/student")
public class StudentServlet extends HttpServlet {
    /**
     * The doPost method handles POST requests.
     *
     * @param request:  Provides client request information for HTTP servlets.
     * @param response: Provides HTTP specific functionality in sending a response.
     * @throws ServletException: If problems are encountered while handling the HttpServletRequest and/or -response,
     *                           a ServletException will be raised.
     * @throws IOException       If an input or output error happens when the servlet handles the GET request, an
     *                           an IOException is thrown.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    /**
     * The doGet method handles GET requests.
     *
     * @param request:  Provides client request information for HTTP servlets.
     * @param response: Provides HTTP specific functionality in sending a response.
     * @throws ServletException: If problems are encountered while handling the HttpServletRequest and/or -response,
     *                           a ServletException will be raised.
     * @throws IOException       If an input or output error happens when the servlet handles the GET request, an
     *                           an IOException is thrown.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String dbUser = getServletContext().getInitParameter("dbUser");
        String dbUrl = getServletContext().getInitParameter("dbUrl");
        String dbPass = getServletContext().getInitParameter("dbPass");
        BioinfConnectorMySQL connector = BioinfConnectorMySQL.getInstance(dbUrl, dbUser, dbPass);

        request.setAttribute("title", "Bioinf.nl - Studenten");

        // FACT OF THE DAY
        FactsMySQL factRequest = FactsMySQL.getInstance(connector.getConnection());
        Message fact = factRequest.getRandomFact();
        request.setAttribute("fact", fact);

        // NEWS
        NewsMySQL newsRequest = NewsMySQL.getInstance(connector.getConnection());
        List<Message> messages = newsRequest.getAllNewsItems();
        request.setAttribute("news", messages);

        // AGENDA
        AgendaMySQL agendaRequest = AgendaMySQL.getInstance(connector.getConnection());
        List<Agenda> agendaItems = agendaRequest.getAllAgendaItems();
        request.setAttribute("events", agendaItems);

        // LINKS AND INFORMATION
        LinkMySQL linkRequest = LinkMySQL.getInstance(connector.getConnection());
        List<Link> linkItems = linkRequest.getAllLinks();
        request.setAttribute("linkItems", linkItems);
        List<Link> informationItems = linkRequest.getAllInformation();
        request.setAttribute("informationItems", informationItems);

        // TEAM
        TeamMySQL teamRequest = TeamMySQL.getInstance(connector.getConnection());
        List<Team> teamMembers = teamRequest.getAllTeamMembers();
        request.setAttribute("team", teamMembers);

        // SET Cookie
        response.addCookie(Cookies.setCookie("student", "yes", request.getCookies()));

        // SET success banner
        String getPar = request.getParameter("feedback");
        if (getPar != null) {
            request.setAttribute("successMsg", "Bedankt voor je feedback, deze is in goede orde ontvangen!");
        }

        RequestDispatcher view = request.getRequestDispatcher("student.jsp");
        view.forward(request, response);
    }
}
