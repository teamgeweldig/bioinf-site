/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.servlets;

import nl.bioinf.database.BioinfConnectorMySQL;
import nl.bioinf.database.CourseMySQL;
import nl.bioinf.database.PromotionBlocksMySQL;
import nl.bioinf.database.SlideshowItemsMySQL;
import nl.bioinf.model.Block;
import nl.bioinf.model.Cookies;
import nl.bioinf.model.Course;
import nl.bioinf.model.SlideshowItem;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The PromotionPageServlet class is used to send dynamic content to the bioinf.nl promotion page.
 * It contains the following methods:
 * doPost(request, response):   handles POST requests.
 * doGet(request, response):    handles GET requests.
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 0.0.1
 */
@WebServlet(name = "PromotionPageServlet", urlPatterns = "/promotion")
public class PromotionPageServlet extends HttpServlet {
    /**
     * The doPost method handles POST requests.
     *
     * @param request:  Provides client request information for HTTP servlets.
     * @param response: Provides HTTP specific functionality in sending a response.
     * @throws ServletException: If problems are encountered while handling the HttpServletRequest and/or -response,
     *                           a ServletException will be raised.
     * @throws IOException       If an input or output error happens when the servlet handles the GET request, an
     *                           an IOException is thrown.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    /**
     * The doGet method handles GET requests.
     *
     * @param request:  Provides client request information for HTTP servlets.
     * @param response: Provides HTTP specific functionality in sending a response.
     * @throws ServletException: If problems are encountered while handling the HttpServletRequest and/or -response,
     *                           a ServletException will be raised.
     * @throws IOException       If an input or output error happens when the servlet handles the GET request, an
     *                           an IOException is thrown.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String dbUser = getServletContext().getInitParameter("dbUser");
        String dbUrl = getServletContext().getInitParameter("dbUrl");
        String dbPass = getServletContext().getInitParameter("dbPass");
        BioinfConnectorMySQL connector = BioinfConnectorMySQL.getInstance(dbUrl, dbUser, dbPass);

        request.setAttribute("title", "Bioinf.nl - Promotie");

        // SLIDESHOW ITEMS
        SlideshowItemsMySQL slideshowItemsRequest = SlideshowItemsMySQL.getInstance(connector.getConnection());

        List<SlideshowItem> slideshowItems1 = slideshowItemsRequest.getLimitedItems(6, 1);
        List<SlideshowItem> slideshowItems2 = slideshowItemsRequest.getLimitedItems(6, 2);

        request.setAttribute("slideshowItems1", slideshowItems1);
        request.setAttribute("slideshowItems2", slideshowItems2);


        // PROMOTION BLOCKS
        PromotionBlocksMySQL promotionBlocksRequest = PromotionBlocksMySQL.getInstance(connector.getConnection());

        List<Block> promotionBlocks = promotionBlocksRequest.getAllPromotionBlocks();

        request.setAttribute("promotionBlocks", promotionBlocks);

        // COURSES
        CourseMySQL CourseRequest = CourseMySQL.getInstance(connector.getConnection());
        List<Course> courseItems = CourseRequest.getAllCourseItems();
        request.setAttribute("courseItems", courseItems);

        // SET Cookie
        response.addCookie(Cookies.setCookie("student", "no", request.getCookies()));

        RequestDispatcher view = request.getRequestDispatcher("promotion.jsp");

        view.forward(request, response);

    }
}