/*
 * Copyright (c) 2018 Beau Elting, Kimberley Kalkhoven, Sven Hakvoort & Wietse Reitsma.
 * All rights reserved.
 */
package nl.bioinf.servlets;

import nl.bioinf.model.Cookies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * Controller servlet for determining to which location the visitor needs to be directed
 *
 * @author Beau Elting, Kimberley Kalkhoven, Sven Hakvoort and Wietse Reitsma
 * @version 1.0
 */
@WebServlet(name = "nl.bioinf.servlets.Servlet", urlPatterns = "")
public class Servlet extends HttpServlet {
    /**
     * The doPost method handles POST requests.
     *
     * @param request:  Provides client request information for HTTP servlets.
     * @param response: Provides HTTP specific functionality in sending a response.
     * @throws ServletException: If problems are encountered while handling the HttpServletRequest and/or -response,
     *                           a ServletException will be raised.
     * @throws IOException       If an input or output error happens when the servlet handles the GET request, an
     *                           an IOException is thrown.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    /**
     * The doGet method handles GET requests.
     *
     * @param request:  Provides client request information for HTTP servlets.
     * @param response: Provides HTTP specific functionality in sending a response.
     * @throws ServletException: If problems are encountered while handling the HttpServletRequest and/or -response,
     *                           a ServletException will be raised.
     * @throws IOException       If an input or output error happens when the servlet handles the GET request, an
     *                           an IOException is thrown.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (Cookies.isSet("student", request.getCookies())) {
            if (Objects.equals(Cookies.getCookie("student", request.getCookies()).getValue(), "yes")) {
                response.sendRedirect("student");
            } else {
                response.sendRedirect("promotion");
            }
        } else {
            response.sendRedirect("promotion");
        }
    }
}
