-- Database used for the web application
-- Comments are in Dutch -> generated by phpMyAdmin

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `agenda`
--

DROP TABLE IF EXISTS `agenda`;
CREATE TABLE IF NOT EXISTS `agenda` (
  `id`          INT(11)      NOT NULL AUTO_INCREMENT,
  `title`       VARCHAR(255) NOT NULL,
  `date`        VARCHAR(255) NOT NULL,
  `description` TEXT,
  `location`    VARCHAR(255)          DEFAULT NULL,
  `image`       VARCHAR(255)          DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `blockInfo`
--

DROP TABLE IF EXISTS `blockInfo`;
CREATE TABLE IF NOT EXISTS `blockInfo` (
  `id`     TINYINT(4) NOT NULL,
  `width`  INT(11) DEFAULT NULL,
  `height` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `facts`
--

DROP TABLE IF EXISTS `facts`;
CREATE TABLE IF NOT EXISTS `facts` (
  `id`      INT(11)      NOT NULL AUTO_INCREMENT,
  `title`   VARCHAR(255)          DEFAULT NULL,
  `message` VARCHAR(255) NOT NULL,
  `image`   VARCHAR(255)          DEFAULT NULL,
  `source`  TEXT,
  PRIMARY KEY (`id`)
);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `links`
--

DROP TABLE IF EXISTS `links`;
CREATE TABLE IF NOT EXISTS `links` (
  `id`      INT(11)    NOT NULL AUTO_INCREMENT,
  `type`    TINYINT(1) NOT NULL,
  `name`    VARCHAR(50)         DEFAULT NULL,
  `address` TEXT,
  `image`   TEXT,
  PRIMARY KEY (`id`)
);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id`      INT(11)   NOT NULL AUTO_INCREMENT,
  `content` TEXT      NOT NULL,
  `date`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `author`  VARCHAR(255)       DEFAULT NULL,
  `title`   VARCHAR(255)       DEFAULT NULL,
  `images`  VARCHAR(255)       DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `promotionBlocks`
--

DROP TABLE IF EXISTS `promotionBlocks`;
CREATE TABLE IF NOT EXISTS `promotionBlocks` (
  `id`               INT(11)      NOT NULL AUTO_INCREMENT,
  `title`            VARCHAR(100)          DEFAULT NULL,
  `subtitle`         VARCHAR(100)          DEFAULT NULL,
  `content`          TEXT,
  `imageFileName`    VARCHAR(300) NOT NULL,
  `link`             VARCHAR(100)          DEFAULT NULL,
  `promotionBlockId` TINYINT(4)            DEFAULT NULL,
  `date`             TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`promotionBlockId`) REFERENCES `blockInfo` (`id`)
);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `slideshowItems`
--

DROP TABLE IF EXISTS `slideshowItems`;
CREATE TABLE IF NOT EXISTS `slideshowItems` (
  `id`            INT(11)      NOT NULL AUTO_INCREMENT,
  `title`         VARCHAR(100)          DEFAULT NULL,
  `subtitle`      VARCHAR(100)          DEFAULT NULL,
  `imageFileName` VARCHAR(100) NOT NULL,
  `link`          VARCHAR(100)          DEFAULT NULL,
  `priority`      TINYINT(4)            DEFAULT NULL,
  `slideshowId`   TINYINT(4)            DEFAULT NULL,
  `date`          TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `team`
--

DROP TABLE IF EXISTS `team`;
CREATE TABLE IF NOT EXISTS `team` (
  `id`           INT(11)      NOT NULL AUTO_INCREMENT,
  `firstName`    VARCHAR(50)  NOT NULL,
  `lastName`     VARCHAR(50)  NOT NULL,
  `subject`      VARCHAR(255) NOT NULL,
  `images`       VARCHAR(255)          DEFAULT NULL,
  `email`        VARCHAR(255) NOT NULL,
  `abbreviation` VARCHAR(4)            DEFAULT NULL,
  `room`         VARCHAR(20)  NOT NULL,
  `info`         TEXT,
  PRIMARY KEY (`id`)
);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Course`
--

DROP TABLE IF EXISTS `courses`;
CREATE TABLE IF NOT EXISTS `courses` (
  `id`                 INT(11) NOT NULL AUTO_INCREMENT,
  `title`              VARCHAR(20)      DEFAULT NULL,
  `subjects`           TEXT,
  `description`        TEXT,
  `image`              TEXT,
  `subjectDescription` TEXT,
  PRIMARY KEY (`id`)
);


