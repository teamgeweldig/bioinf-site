# Bioinf site

New website for the Bioinformatics study of the Hanze University of Applied Sciences

## Authors ##

Kimberley Kalkhoven  
k.l.kalkhoven@st.hanze.nl

Beau Elting  
eltingbeau@gmail.com

Wietse Reitsma  
wietse4@gmail.com

Sven Hakvoort   
info@hakvoortdevelopment.nl

## Purpose ##

The purpose of this web application is to provide a good looking website for the Bioinformatics study in which it is
easy to find information about the study. The website features a promotion page for people looking for information about the study, which can help them
determine if this could be something for them. As well as an information page for current students. The student page gives
a good overview of upcoming events, news and other useful information.

## Dependencies ##

- Apache tomcat, available from (http://tomcat.apache.org/)
- MySQL database

## Installation ##

In order to set up this web application you will first need to clone the repo and make sure the dependencies are installed and set up.

The configuration for this web application is defined in the web.xml file (/src/main/webapp/WEB-INF/web.xml), in order for this
application to work the right paths, url's and usernames need to be entered here.

The database (DB) settings require the url of the DB, the username and password and the name of the database to use.
It is recommended to add '?useUnicode=true&amp;characterEncoding=UTF-8' at the end of the URL to support special characters.

The image paths are required for images saved in the 'img' folder when filenames are entered in the DB instead of a http link.

The SMTP settings require the information for your email account in order to use the support form.

In order to set up the database execute the code in database.sql (/src/main/sql/database.sql) on the database you want to use.
This will set up the required tables for the application.

You can also execute dummyData.sql to use some predefined data.

## Adding information ##

Adding new information is pretty straight forward, however there are a few special cases which will be explained here.

For example for a new fact:  
```
INSERT INTO facts (id, title, message, image, source) VALUES
(1, 'Title of fact', 'Content of fact', 'link to image', 'source example');
```

BlockInfo:

In this table the id refers to the promotionBlockId in promotionBlocks allowing to set the width and height of the specified block.

For example:  
```
INSERT INTO promotionBlocks (id, title, subtitle, content, imageFileName, link, promotionBlockId, date) VALUES
(1, 'Title example', 'Subtitle example', 'Content of promotion block',
 'link to image', '', 3, '2018-01-11 11:01:34');
```

Links:

The type column determines in which block the link needs to be placed, 1 for left and 2 for right.

For example:  
```
INSERT INTO links (id, type, name, address, image) VALUES
(1, 1, 'Link title', 'Link example', 'Link to image');
```

Courses:

This table allows for a description of the subjects, this needs to be put in the column subjectDescription in JSON format.
The key needs to be EXACTLY the same as the subject name entered in subjects (needs to be comma separated) and the value is the description.

Subjects column NEEDS to be comma separated.

For example:  
```
INSERT INTO courses (id, title, subjects, description, image, vakOmschrijving) VALUES
(1, 'Course 1', 'Biology 1, Informatics 1, Mathematics 1, Course assignment ',
 'Course-opdracht: Course description.', 'link to image',
 '{''Biology 1'':''Biology 1 description''}');
```


