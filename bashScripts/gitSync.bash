#!/bin/bash
staging=$1
dev=$2

if [ -z $dev ] || [ -z $staging ]
	then
	echo "Usage: gitSync.bash <current branch> <new branch to create>"
else
git fetch
git checkout $staging
git pull

git checkout $dev
git merge $staging
git push -u origin $dev

git credential-cache exit
fi
