#!/bin/bash
staging=$1
dev=$2

if [ -z $staging ] || [ -z $dev ]
        then
        echo "Usage: gitMerge.bash <target branch> <branch to merge into target>"
else
git config --global credential.helper cache

git fetch
git checkout $dev
git branch -u origin/$dev
git pull
git add .
git commit -m "Prepare $dev for merge"
git push

git checkout $staging
git branch -u origin/$staging
git pull
git merge --no-ff $dev
git fetch
git add .
git commit -m "Merged $staging and $dev"
git push
git fetch
git push origin --delete $dev
git fetch
fi
